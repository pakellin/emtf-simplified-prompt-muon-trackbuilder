CC    = g++

OPT   = -I include_hls_ap/ -o emtf.o -std=c++11
CVP13_OPT   = -I include_hls_ap/ -o emtf.o -std=c++11

SP_FILES = test_emtf.cpp 
TF_FILES = test_emtf_tf_core.cpp 
PC_FILES = emtf_core/PrimitiveConversion/test_prim_conv.cpp 
CVP13_FILES = cvp13_testing/emtf_cvp13_test.cpp


sp: $(SP_FILES)
	$(CC) $(OPT) $? 

pc: $(PC_FILES)
	$(CC) $(OPT) $? 

trk_finder: $(TF_FILES)
	$(CC) $(OPT) $? 

# For cvp13 testing
cvp13: $(CVP13_FILES)
	$(CC) $(CVP13_OPT) $? 

all:
	sp

clean:
	rm *.o
