// Top function
#include "emtf_core/tf_core.h"

#include <fstream>
#include "testing_software/load_event.h"
#include "testing_software/random_segment_generator.h"
#include "testing_software/convert_inputs.h"
#include "testing_software/compare_event_features.h"
#include "testing_software/compare_event_invpts.h"
#include "testing_software/write_invpt_output.h"


// Main driver
int main(int argc, char** argv) {

    bool use_random_segments = 0; // else use files
    bool use_old_style_inputs = 0;

    // List of event numbers
    //std::initializer_list<int> event_list = {583, 603, 749, 852}; // For specific files
    std::vector<int> event_list(2369);
    std::iota(event_list.begin(), event_list.end(), 0);


    // Loop over events
    for (auto ievt : event_list) {
        std::cout << "\033[1;34m" << "Processing event " << ievt << "\033[0m" << std::endl; // color header blue


        // Initialize input & output
        emtf_seg_t emtf_segs_in[num_sites][max_ch_site][max_seg_ch];
        trk_feat_t trk_features_out[num_tracks][num_emtf_features];
        trk_feat_t trk_features_rm_out[num_tracks][num_emtf_features];
        trk_invpt_t trk_invpts_out[num_tracks];


        // Fil the input
        if(use_old_style_inputs){
            emtf_seg_t in0[230];
            if(use_random_segments)
                random_segment_generator(in0); // Use random events
            else
                load_event_old(ievt, in0); // Use file input
            convert_inputs(in0, emtf_segs_in);
        }
        else{
            // For random numbers use old (non-site) style
            load_event(ievt, emtf_segs_in);
        }


        // Call the top function !!
        trk_finding_core(0,emtf_segs_in, trk_features_out, trk_features_rm_out, trk_invpts_out);


        // Test Event if using input files (must have corresponding result file for feats and invpt)
        if(!use_random_segments){
            compare_event_features(ievt, trk_features_out);
            compare_event_invpts(ievt, trk_invpts_out);
        }
        else{
            // Just to output something
            for(int trk=0; trk<4; trk++)
                std::cout << trk_invpts_out[trk] << ", ";
            std::cout << "\n";
        }

        
        //write_invpt_output(ievt, trk_invpts_out);

    }  // end loop over events


    return 0;

}
