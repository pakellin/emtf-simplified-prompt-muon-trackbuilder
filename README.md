EMTF Mini Emulator - used mainly for testing firmware

Uses Arbitrary Precion Types in included folder.
To match hls NN output, a table generated using Vitis HLS for the tanh activation function is also included.

To Run:
make;
./emtf.o
