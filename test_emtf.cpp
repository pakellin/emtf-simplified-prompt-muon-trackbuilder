#include "emtf_core/sp.h"

#include "testing_software/random_tp_generator.h"


// Main driver
int main(int argc, char** argv) {

    // Trigger Primitives
    csc_lct_t       csc_lcts[6][9][seg_ch_csc];
    gem_cluster_t   ge11_cl[7][gem_layers][seg_ch_gem];
    gem_cluster_t   ge21_cl[4][gem_layers][seg_ch_gem];
    rpc_cluster_t   rpc_cl[7][3][2][seg_ch_rpc];
    ap_uint<7>      rpc_link_vl;
    irpc_cluster_t  irpc_cl[2][4][seg_ch_irpc];
    me0_lct_t       me0_lcts[5][seg_ch_me0];

    const int n_events = 1;


    // Run through the events
    for(int evt=0; evt<n_events; evt++){

        // Zero all input elements
        std::memset(csc_lcts, 0, sizeof csc_lcts);
        std::memset(ge11_cl, 0, sizeof ge11_cl);
        std::memset(ge21_cl, 0, sizeof ge21_cl);
        std::memset(rpc_cl, 0, sizeof rpc_cl);
        rpc_link_vl = 0;
        std::memset(irpc_cl, 0, sizeof irpc_cl);
        std::memset(me0_lcts, 0, sizeof me0_lcts);


        csc_lcts[0][0][0].hs = 202;
        csc_lcts[0][0][0].wg = 8;
        csc_lcts[0][0][0].cp = 8;
        csc_lcts[0][0][0].ql = 6;
        csc_lcts[0][0][0].vf = 1;

        csc_lcts[2][0][0].hs = 48;
        csc_lcts[2][0][0].wg = 29;
        csc_lcts[2][0][0].cp = 8;
        csc_lcts[2][0][0].ql = 6;
        csc_lcts[2][0][0].vf = 1;

        csc_lcts[3][0][0].hs = 118;
        csc_lcts[3][0][0].wg = 34;
        csc_lcts[3][0][0].cp = 8;
        csc_lcts[3][0][0].ql = 6;
        csc_lcts[3][0][0].vf = 1;

        csc_lcts[4][0][0].hs = 122;
        csc_lcts[4][0][0].wg = 35;
        csc_lcts[4][0][0].cp = 8;
        csc_lcts[4][0][0].ql = 6;
        csc_lcts[4][0][0].vf = 1;

        ge11_cl[1][0][0].pad = 27;
        ge11_cl[1][0][0].roll = 4;
        ge11_cl[1][0][0].cl_sz = 2;
        ge11_cl[1][0][0].valid = 1;
        ge11_cl[1][1][0].pad = 27;
        ge11_cl[1][1][0].roll = 4;
        ge11_cl[1][1][0].cl_sz = 2;
        ge11_cl[1][1][0].valid = 1;

        ge21_cl[1][0][0].pad = 118;
        ge21_cl[1][0][0].roll = 5;
        ge21_cl[1][0][0].cl_sz = 2;
        ge21_cl[1][0][0].valid = 1;
        ge21_cl[1][1][0].pad = 117;
        ge21_cl[1][1][0].roll = 5;
        ge21_cl[1][1][0].cl_sz = 2;
        ge21_cl[1][1][0].valid = 1;

        me0_lcts[1][0].halfstrip = 232;
        me0_lcts[1][0].roll = 2;
        me0_lcts[1][0].bend = 22;
        me0_lcts[1][0].qual = 4;
        me0_lcts[1][0].valid = 1;

        // Create random events - must pre-zero these
        //random_segment_generator( csc_lcts, ge11_cl, ge21_cl, rpc_cl, rpc_link_vl, irpc_cl, me0_lcts);

        int endcap = 1;
        int sector = 1;


        trk_feat_t trk_features_out[num_tracks][num_emtf_features];
        trk_feat_t trk_features_rm_out[num_tracks][num_emtf_features];
        trk_invpt_t trk_invpts_out[num_tracks];

        sector_processor(endcap, sector, csc_lcts, ge11_cl, ge21_cl, rpc_cl, rpc_link_vl, irpc_cl, me0_lcts, trk_features_out, trk_features_rm_out, trk_invpts_out);

        
        for(int trk=0; trk<4; trk++){
            for(int feat=0; feat<40; feat++)
                std::cout << trk_features_out[trk][feat] << ", ";
            std::cout << "\n";
        }
        std::cout << "\n";

        // Just to output something
        for(int trk=0; trk<4; trk++)
            std::cout << trk_invpts_out[trk] << ", ";
        std::cout << "\n";
    }


    return 0;

}
