#ifndef __PROMPT_TRKBUILD_CONVERT_INPUTS_H__
#define __PROMPT_TRKBUILD_CONVERT_INPUTS_H__

#include <random>
#include "../emtf_core/common.h"
#include "../emtf_core/types.h"

void convert_inputs(  emtf_seg_t in0[230], emtf_seg_t emtf_segs[num_sites][max_ch_site][max_seg_ch]){
    // Re-ordered to put neighbors first
    int ch_ids[num_sites][max_ch_site] = {
        {114,108, 109, 110, 111, 112, 113},     // ME0      // This will change (only 5 chambers)
        {99,  54,  55,  56,  63,  64,  65},     // GE11
        {45,   0,   1,   2,   9,  10,  11},     // ME11
        {46,   3,   4,   5,  12,  13,  14},     // ME12
        {100, 57,  58,  59,  66,  67,  68},     // RE12
        {102, 72,  73,  74,  -1,  -1,  -1},     // GE21
        {103, 75,  76,  77,  78,  79,  80},     // RE22
        {48,  18,  19,  20,  -1,  -1,  -1},     // ME21
        {49,  21,  22,  23,  24,  25,  26},     // ME22
        {50,  27,  28,  29,  -1,  -1,  -1},     // ME31
        {51,  30,  31,  32,  33,  34,  35},     // ME32
        {104, 81,  82,  83,  -1,  -1,  -1},     // RE31
        {105, 84,  85,  86,  87,  88,  89},     // RE32
        {52,  36,  37,  38,  -1,  -1,  -1},     // ME41
        {53,  39,  40,  41,  42,  43,  44},     // ME42
        {106, 90,  91,  92,  -1,  -1,  -1},     // RE41
        {107, 93,  94,  95,  96,  97,  98}      // RE42
    };


    // Zero everything
    for(int site=0; site<num_sites; site++){
        for(int ch=0; ch<max_ch_site; ch++){
            for(int seg=0; seg<max_seg_ch; seg++){
                emtf_segs[site][ch][seg].emtf_phi    = 0;
                emtf_segs[site][ch][seg].emtf_theta1 = 0;
                emtf_segs[site][ch][seg].emtf_theta2 = 0;
                emtf_segs[site][ch][seg].emtf_bend   = 0;
                emtf_segs[site][ch][seg].emtf_qual   = 0;
                emtf_segs[site][ch][seg].emtf_time   = 0;
                emtf_segs[site][ch][seg].seg_zones  = 0;
                emtf_segs[site][ch][seg].seg_bx     = 0;
                emtf_segs[site][ch][seg].seg_valid  = 0;
            }
        }
    }


    int me0_seg_nums[5] = {0, 0, 0, 0, 0};
    // Load valid segments
    for(int site=0; site<num_sites; site++){
        for(int ch=0; ch<max_ch_site; ch++){
            for(int seg=0; seg<2; seg++){
                int ch_id = ch_ids[site][ch];

                if(site==0 && type_by_site[site] == twenty_me0){
                    // Handle ME0
                    int real_ch;
                    int real_seg;

                    int phi_col = (in0[2*ch_id + seg].emtf_phi >> 4);
                        
                    if(phi_col <= 56)
                        real_ch = 0;
                    else if(phi_col <= 131)
                        real_ch = 1;
                    else if(phi_col <= 206)
                        real_ch = 2;
                    else if(phi_col <= 281)
                        real_ch = 3;
                    else
                        real_ch = 4;

                    real_seg = me0_seg_nums[real_ch]; // get next segment number

                    if(in0[2*ch_id + seg].seg_valid && real_seg < max_seg_ch){
                        // Only increment for valid segments
                        me0_seg_nums[real_ch]++;

                        emtf_segs[site][real_ch][real_seg].emtf_phi    = in0[2*ch_id + seg].emtf_phi;
                        emtf_segs[site][real_ch][real_seg].emtf_theta1 = in0[2*ch_id + seg].emtf_theta1;
                        emtf_segs[site][real_ch][real_seg].emtf_theta2 = in0[2*ch_id + seg].emtf_theta2;
                        emtf_segs[site][real_ch][real_seg].emtf_bend   = in0[2*ch_id + seg].emtf_bend;
                        emtf_segs[site][real_ch][real_seg].emtf_qual   = in0[2*ch_id + seg].emtf_qual;
                        emtf_segs[site][real_ch][real_seg].emtf_time   = in0[2*ch_id + seg].emtf_time;
                        emtf_segs[site][real_ch][real_seg].seg_zones  = in0[2*ch_id + seg].seg_zones;
                        emtf_segs[site][real_ch][real_seg].seg_bx     = in0[2*ch_id + seg].seg_bx;
                        emtf_segs[site][real_ch][real_seg].seg_valid  = in0[2*ch_id + seg].seg_valid;
                    }


                }
                else if(ch_id != -1){
                    emtf_segs[site][ch][seg].emtf_phi    = in0[2*ch_id + seg].emtf_phi;
                    emtf_segs[site][ch][seg].emtf_theta1 = in0[2*ch_id + seg].emtf_theta1;
                    emtf_segs[site][ch][seg].emtf_theta2 = in0[2*ch_id + seg].emtf_theta2;
                    emtf_segs[site][ch][seg].emtf_bend   = in0[2*ch_id + seg].emtf_bend;
                    emtf_segs[site][ch][seg].emtf_qual   = in0[2*ch_id + seg].emtf_qual;
                    emtf_segs[site][ch][seg].emtf_time   = in0[2*ch_id + seg].emtf_time;
                    emtf_segs[site][ch][seg].seg_zones  = in0[2*ch_id + seg].seg_zones;
                    emtf_segs[site][ch][seg].seg_bx     = in0[2*ch_id + seg].seg_bx;
                    emtf_segs[site][ch][seg].seg_valid  = in0[2*ch_id + seg].seg_valid;
                }
            }
        }
    }


    //#define OUTPUT_FOR_FIRMWARE
    #ifdef OUTPUT_FOR_FIRMWARE
        std::ofstream of;
        std::stringstream filename;
        static int event=0;
        filename << "emtf_tb_data_me0_fix/event_" << event << ".txt";
        event++;
        of.open(filename.str());
        if(of.is_open()){
            for(int site=0; site<num_sites; site++){
                for(int ch=0; ch<max_ch_site; ch++){
                    for(int seg=0; seg<max_seg_ch; seg++){
                        int ch_id = ch_ids[site][ch];

                        if(ch_id != -1 && seg<2 && emtf_segs[site][ch][seg].seg_valid){
                            
                            of << site << ", " << ch << ", " << seg << ", " << 
                                emtf_segs[site][ch][seg].emtf_phi << ", " <<
                                emtf_segs[site][ch][seg].emtf_theta1 << ", " <<
                                emtf_segs[site][ch][seg].emtf_theta2 << ", " <<
                                emtf_segs[site][ch][seg].emtf_bend << ", " <<
                                emtf_segs[site][ch][seg].emtf_qual << ", " <<
                                emtf_segs[site][ch][seg].seg_zones << "\n";

                        } 
                    }
                }
            }
        }
        of.close();
    #endif
}

#endif
