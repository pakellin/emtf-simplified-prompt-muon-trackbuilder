#ifndef __PROMPT_TRKBUILD_LOAD_EVENT_H__
#define __PROMPT_TRKBUILD_LOAD_EVENT_H__

#include <random>
#include "../emtf_core/common.h"
#include "../emtf_core/types.h"


void load_event(const int ievent, emtf_seg_t emtf_segs[num_sites][max_ch_site][max_seg_ch]){

    for(int site=0; site<num_sites; site++){
        for(int ch=0; ch<max_ch_site; ch++){
            for(int seg=0; seg<max_seg_ch; seg++){
                emtf_segs[site][ch][seg].emtf_phi = 0;
                emtf_segs[site][ch][seg].emtf_bend = 0;
                emtf_segs[site][ch][seg].emtf_theta1 = 0;
                emtf_segs[site][ch][seg].emtf_theta2 = 0;
                emtf_segs[site][ch][seg].emtf_qual = 0;
                emtf_segs[site][ch][seg].emtf_time = 0;
                emtf_segs[site][ch][seg].seg_zones = 0;
                emtf_segs[site][ch][seg].seg_bx = 0;
                emtf_segs[site][ch][seg].seg_valid = 0;
            }
        }
    }

    std::stringstream filename;
    filename << "emtf_tb_data/emtf_input_data_me0_fix/event_" << ievent << ".txt";
    std::ifstream myfile; 
    myfile.open(filename.str());

    const int N_VALS = 9;

    char comma;
    int val[N_VALS];

    if ( myfile.is_open() ) { // always check whether the file is open
        while(!myfile.eof()){
            for(int i=0; i<N_VALS; i++){
                myfile >> val[i]; 
                if(i != N_VALS-1)
                    myfile >> comma;
            }

            // Add values to emtf input
            int site = val[0];
            int ch = val[1];
            int seg = val[2];

            emtf_segs[site][ch][seg].emtf_phi = val[3];
            emtf_segs[site][ch][seg].emtf_theta1 = val[4];
            emtf_segs[site][ch][seg].emtf_theta2 = val[5];
            emtf_segs[site][ch][seg].emtf_bend = val[6];
            emtf_segs[site][ch][seg].emtf_qual = val[7];
            emtf_segs[site][ch][seg].seg_zones = val[8];
            emtf_segs[site][ch][seg].seg_valid = 1;
        }

        myfile.close();
    }else{
        std::cout << "Failed to open load file." << std::endl;
    }
}



void load_event_old(const int ievent, emtf_seg_t in0[230]){
    for(int seg=0; seg<230; seg++){
        in0[seg].emtf_phi = 0;
        in0[seg].emtf_bend = 0;
        in0[seg].emtf_theta1 = 0;
        in0[seg].emtf_theta2 = 0;
        in0[seg].emtf_qual = 0;
        in0[seg].emtf_time = 0;
        in0[seg].seg_zones = 0;
        in0[seg].seg_bx = 0;
        in0[seg].seg_valid = 0;
    }

    std::stringstream filename;
    filename << "emtf_tb_data/emtf_input_data/event_" << ievent << ".txt";
    std::ifstream myfile; 
    myfile.open(filename.str());

    const int N_VALS = 11;

    char comma;
    int val[N_VALS];

    if ( myfile.is_open() ) { // always check whether the file is open
        while(!myfile.eof()){
            for(int i=0; i<N_VALS; i++){
                myfile >> val[i]; 
                myfile >> comma;
            }

            // Add values to emtf input
            int seg = val[0];
            in0[seg].emtf_phi = val[1];
            in0[seg].emtf_theta1 = val[2];
            in0[seg].emtf_theta2 = val[3];
            in0[seg].emtf_bend = val[4];
            in0[seg].emtf_qual = val[5];
            in0[seg].emtf_time = val[6];
            in0[seg].seg_zones = val[7];
            in0[seg].seg_bx = val[9];
            in0[seg].seg_valid = val[10];
        }

        myfile.close();
    }else{
        std::cout << "Failed to open load file." << std::endl;
    }
}


#endif
