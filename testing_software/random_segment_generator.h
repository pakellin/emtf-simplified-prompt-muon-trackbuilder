#ifndef __PROMPT_TRKBUILD_RANDOM_SEG_GEN_H__
#define __PROMPT_TRKBUILD_RANDOM_SEG_GEN_H__

#include <random>
#include "../emtf_core/common.h"
#include "../emtf_core/types.h"

const int chamber_theta_ranges[17][2] = {
    {4,  23}, // ME0
    {17, 52}, // GE11
    {4,  53}, // ME11
    {46, 88}, // ME12
    {52, 84}, // RE12
    {7,  46}, // GE21
    {52, 88}, // RE22
    {4,  49}, // ME21
    {52, 88}, // ME22
    {4,  41}, // ME31
    {44, 88}, // ME32
    {4,  36}, // RE31
    {40, 84}, // RE32
    {4,  35}, // ME41
    {38, 88}, // ME42
    {4,  31}, // RE41
    {35, 84}  // RE42
};


const int site_zone_ranges[17][3][2] = { // [site][zone][min,max]
    {{ 4, 23}, {-1, -1}, {-1, -1}},   // ME0
    {{17, 26}, {24, 52}, {-1, -1}}, // GE11
    {{ 4, 26}, {24, 53}, {-1, -1}}, // ME11
    {{-1, -1}, {46, 54}, {52, 88}}, // ME12
    {{-1, -1}, {52, 56}, {52, 84}}, // RE12
    {{ 7, 25}, {23, 46}, {-1, -1}}, // GE21
    {{-1, -1}, {-1, -1}, {52, 88}}, // RE22
    {{ 4, 25}, {23, 49}, {-1, -1}}, // ME21
    {{-1, -1}, {-1, -1}, {52, 88}}, // ME22
    {{ 4, 25}, {23, 41}, {-1, -1}}, // ME31
    {{-1, -1}, {44, 54}, {50, 88}}, // ME32
    {{ 4, 25}, {23, 36}, {-1, -1}}, // RE31
    {{-1, -1}, {40, 52}, {48, 84}}, // RE32
    {{ 4, 25}, {23, 35}, {-1, -1}}, // ME41
    {{-1, -1}, {38, 54}, {50, 88}}, // ME42
    {{ 4, 25}, {23, 31}, {-1, -1}}, // RE41
    {{-1, -1}, {35, 54}, {52, 84}}  // RE42
};



void get_site_num(int iseg, int site_and_ch_idx[2]){
    int ch_id = int(iseg/2);

    // Re-ordered to put neighbors first
    const int ch_ids[num_sites][max_ch_site] = {
        {114,108, 109, 110, 111, 112, 113},     // ME0      // This will change (only 5 chambers)
        {99,  54,  55,  56,  63,  64,  65},     // GE11
        {45,   0,   1,   2,   9,  10,  11},     // ME11
        {46,   3,   4,   5,  12,  13,  14},     // ME12
        {100, 57,  58,  59,  66,  67,  68},     // RE12
        {102, 72,  73,  74,  -1,  -1,  -1},     // GE21
        {103, 75,  76,  77,  78,  79,  80},     // RE22
        {48,  18,  19,  20,  -1,  -1,  -1},     // ME21
        {49,  21,  22,  23,  24,  25,  26},     // ME22
        {50,  27,  28,  29,  -1,  -1,  -1},     // ME31
        {51,  30,  31,  32,  33,  34,  35},     // ME32
        {104, 81,  82,  83,  -1,  -1,  -1},     // RE31
        {105, 84,  85,  86,  87,  88,  89},     // RE32
        {52,  36,  37,  38,  -1,  -1,  -1},     // ME41
        {53,  39,  40,  41,  42,  43,  44},     // ME42
        {106, 90,  91,  92,  -1,  -1,  -1},     // RE41
        {107, 93,  94,  95,  96,  97,  98}      // RE42
    };

    // Return Site Number
    for(int site=0; site<num_sites; site++)
        for(int ch=0; ch<max_ch_site; ch++)
            if(ch_id == ch_ids[site][ch]){
                site_and_ch_idx[0] = site;
                site_and_ch_idx[1] = ch;
            }
}



void random_segment_generator(emtf_seg_t out[230])
{

    for(int iseg=0; iseg<230; iseg++){
        out[iseg].emtf_phi = 0;
        out[iseg].emtf_bend = 0;
        out[iseg].emtf_theta1 = 0;
        out[iseg].emtf_theta2 = 0;
        out[iseg].emtf_qual = 0;
        out[iseg].emtf_time = 0;
        out[iseg].seg_zones = 0;
        out[iseg].seg_bx = 0;
        out[iseg].seg_valid = 0;
    }
    
    // ph_init reference values
    // [0, 38, 75, 113, 150, 188, 225, 263] -> [0.0, 10.1333, 20.0, 30.1333, 40.0, 50.1333, 60.0, 70.1333] deg
    const int chamber_ph_init_10deg[7]       = {38, 75, 113, 150, 188, 225, 263};
    const int chamber_ph_init_20deg[4]       = {0, 75, 150, 225};

    // These are not inclusive (90 means <90)
    const int chamber_ph_cover_10deg[7]      = {90, 127, 165, 202, 240, 277, 315};
    const int chamber_ph_cover_20deg[4]      = {90, 165, 240, 315};

    const int num_twenty_deg_sites = 6;
    const int twenty_deg_sites[num_twenty_deg_sites] = {5, 7, 9, 11, 13, 15};

    const int num_non_csc_sites = 9;
    const int non_csc_chambers[num_non_csc_sites] = {0,1,4,5,6,11,12,15,16};


    static std::mt19937_64 gen64;

    int num_segs = gen64() % 230;

    // Get random number of segments between 1-100
    for(int seg=0; seg<num_segs; seg++){

        // For each segment get a random segment number (you can just overwrite segs if you draw multiple of the same)
        int iseg = gen64() % 230;

        int site_and_ch_idx[2] = {-1, -1};
        get_site_num(iseg, site_and_ch_idx);

        int site_idx = site_and_ch_idx[0];
        int ch_idx = site_and_ch_idx[1];

        if(site_idx == -1 || ch_idx == -1)
            continue; // ME13, skip it

        
        // Get if chamber is 10deg or 20deg
        int ch_type = 0; // 10deg 
        for(int i=0; i<num_twenty_deg_sites; i++)
            if(site_idx == twenty_deg_sites[i])
                ch_type = 1;


        // For each segment, generate random phi within chamber range
        unsigned segment_phi;
        if(ch_type == 0){
            unsigned ch_start_col = chamber_ph_init_10deg[ch_idx];
            unsigned ch_start_phi = ch_start_col << 4;
            int ch_width = 52;
            segment_phi = ch_start_phi + (gen64() % (((ch_width-1) << 4) + 15)); // phi value can cover whole chamber range
        }
        else{
            unsigned ch_start_col = chamber_ph_init_20deg[ch_idx];
            unsigned ch_start_phi = ch_start_col << 4;
            int ch_width = 90;
            segment_phi = ch_start_phi + (gen64() % (((ch_width-1) << 4) + 15)); // phi value can cover whole chamber range
        }
             

        // For each segment, generate a random theta, if it is a csc, potentially generate another theta (say 10% of the time)
        int th_min = chamber_theta_ranges[site_idx][0];
        int th_max = chamber_theta_ranges[site_idx][1];

        bool is_csc = 1;
        for(int i=0; i<num_non_csc_sites; i++)
            if(site_idx == non_csc_chambers[i])
                is_csc = 0;

        int theta1 = th_min + (gen64() % (th_max-th_min));
        int theta2;

        // 10% of theta2 values for csc chambers are set 
        if(is_csc && (gen64() % 10) == 1)
            theta2 = th_min + (gen64() % (th_max-th_min));
        else
            theta2 = 0;


        
        // Attach approprate zone bits to the theta value
        seg_zones_t zones = 0;

        for(int zn=0; zn<3; zn++){
            int zone_min_th = site_zone_ranges[site_idx][zn][0];
            int zone_max_th = site_zone_ranges[site_idx][zn][1];

            if(zone_min_th != -1 && 
                ((theta1 != 0 && theta1 >= zone_min_th && theta1 <= zone_max_th) ||
                (theta2 != 0 && theta2 >= zone_min_th && theta2 <= zone_max_th))
              )
            {

                zones[2-zn] = 1; // Set zones bit
            }
        }

        // Set other values (bend and qual don't matter really for this)
        int bend = (gen64() % 10) - 5;
        int qual = (gen64() % 10);

        out[iseg].emtf_phi = static_cast<emtf_phi_t>(segment_phi);
        out[iseg].emtf_theta1 = static_cast<emtf_theta_t>(theta1);
        out[iseg].emtf_theta2 = static_cast<emtf_theta_t>(theta2);
        out[iseg].emtf_bend = static_cast<emtf_bend_t>(bend);
        out[iseg].emtf_qual = static_cast<emtf_qual_t>(qual);
        out[iseg].emtf_time = static_cast<emtf_time_t>(0);
        out[iseg].seg_zones = static_cast<seg_zones_t>(zones);
        out[iseg].seg_bx = 0;
        out[iseg].seg_valid = 1;
    }

}


#endif
