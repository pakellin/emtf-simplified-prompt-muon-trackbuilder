#ifndef __PROMPT_TRKBUILD_COMPARE_EVENT_INVPTS_H__
#define __PROMPT_TRKBUILD_COMPARE_EVENT_INVPTS_H__

#include <fstream>
#include <iostream>
#include "../emtf_core/common.h"
#include "../emtf_core/types.h"

void compare_event_invpts(const int ievent, trk_invpt_t trk_invpts[num_tracks]){


    std::stringstream filename;
    filename << "emtf_tb_data/emtf_duped_invpt_data_out/result_" << ievent << ".txt";
    std::ifstream myfile; 
    myfile.open(filename.str());


    int trk_invpts_expected[num_tracks];
    char comma;

    if ( myfile.is_open() ) { // always check whether the file is open
        while(!myfile.eof()){
            for(int trk=0; trk<num_tracks; trk++){
                myfile >> trk_invpts_expected[trk];
                myfile >> comma;
            }
        }
        myfile.close();
    }else{
        std::cout << "Failed to open emtf invpt output file" << std::endl;
    }


    int passed = 1;
    for(int trk=0; trk<num_tracks; trk++){
        if(trk_invpts[trk] != trk_invpts_expected[trk]){
            std::cout << "Comparison Failed\n";
            passed = 0;
        }
    }

    if(!passed){
        std::cout << "\nGot:\n";
        for(int trk=0; trk<num_tracks; trk++){
            std::cout << trk_invpts[trk] << ", ";
        }

        std::cout << "\nExpected:\n";
        for(int trk=0; trk<num_tracks; trk++){
            std::cout << trk_invpts_expected[trk] << ", ";
        }
    }
}

#endif
