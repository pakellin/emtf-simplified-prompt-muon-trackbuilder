#ifndef __PROMPT_TRKBUILD_COMPARE_EVENT_FEATS_H__
#define __PROMPT_TRKBUILD_COMPARE_EVENT_FEATS_H__

#include <fstream>
#include <iostream>
#include "../emtf_core/common.h"
#include "../emtf_core/types.h"


void compare_event_features(const int ievent, trk_feat_t trk_features[num_tracks][num_emtf_features]){


    int trk_features_expected[num_tracks][num_emtf_features];

    std::stringstream filename;
    filename << "emtf_tb_data/emtf_feat_data_out/result_" << ievent << ".txt";
    std::ifstream myfile; 
    myfile.open(filename.str());


    char comma;

    if ( myfile.is_open() ) { // always check whether the file is open
        while(!myfile.eof()){
            for(int trk=0; trk<num_tracks; trk++){
                for(int feat=0; feat<num_emtf_features; feat++){
                    myfile >> trk_features_expected[trk][feat];
                    myfile >> comma;
                }
            }
        }
        myfile.close();
    }else{
        std::cout << "Failed to open emtf output file" << std::endl;
    }


    int passed = 1;
    for(int trk=0; trk<num_tracks; trk++){
        for(int feat=0; feat<num_emtf_features; feat++){
            if(trk_features[trk][feat] != trk_features_expected[trk][feat]){
                std::cout << "Comparison Failed\n";
                passed = 0;
            }
        }
    }

    if(!passed){
        std::cout << "Got:\n";
        for(int trk=0; trk<num_tracks; trk++){
            for(int feat=0; feat<num_emtf_features; feat++){
                std::cout << trk_features[trk][feat] << ", ";
            }
            std::cout << "\n";
        }

        std::cout << "Expected:\n";
        for(int trk=0; trk<num_tracks; trk++){
            for(int feat=0; feat<num_emtf_features; feat++){
                std::cout << trk_features_expected[trk][feat] << ", ";
            }
            std::cout << "\n";
        }
    }
}


#endif
