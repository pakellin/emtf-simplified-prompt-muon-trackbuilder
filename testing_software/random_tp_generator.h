#ifndef __PROMPT_TRKBUILD_RANDOM_TP_GEN_H__
#define __PROMPT_TRKBUILD_RANDOM_TP_GEN_H__

#include <random>
#include <cassert>
#include "../emtf_core/common.h"
#include "../emtf_core/types.h"
#include "../emtf_core/PrimitiveConversion/pc_types.h"
#include "../emtf_core/PrimitiveConversion/pc_params.h"

const int chamber_strip_ranges[18] = {
    768, // ME0      - halfstrip
    192, // GE11     - pad
    224, // ME11     - 1/2 strip
    160, // ME12     - 1/8th strip
    -1, // RE12     - NOT USED
    384, // GE21     - pad
    -1, // RE22     - NOT USED
    160, // ME21     - 1/8th strip
    160, // ME22     - halfstrip
    160, // ME31     - 1/8th strip
    160, // ME32     - halfstrip
    -1, // RE31     - NOT USED
    -1, // RE32     - NOT USED
    160, // ME41     - 1/8th strip
    160, // ME42     - halfstrip
    -1, // RE41     - NOT USED
    -1,  // RE42     - NOT USED
    -1   // ME13
};

const int chamber_wg_ranges[18] = {
    16, // ME0
    8, // GE11
    48, // ME11
    64, // ME12
    -1, // RE12
    8, // GE21
    -1, // RE22
    112, // ME21
    64, // ME22
    96, // ME31
    64, // ME32
    -1, // RE31
    -1, // RE32
    96, // ME41
    64, // ME42
    -1, // RE41
    -1,  // RE42
    -1   // ME13
};

int get_num_segs(int max=2){

    static std::mt19937_64 gen64;

    int rand_num = gen64() % 100;
    int num_segs = 0;

    // controls percentage of filled segments (5% have 2 segs, 20% have at least one)
    if(rand_num < 0) // 5%
        num_segs = 2;
    else if(rand_num < 5) // 20% 
        num_segs = 1;

    return num_segs;
}


void random_segment_generator(
        csc_lct_t       csc_lcts[6][9][seg_ch_csc],
        gem_cluster_t   ge11_cl[7][gem_layers][seg_ch_gem],
        gem_cluster_t   ge21_cl[4][gem_layers][seg_ch_gem],
        rpc_cluster_t   rpc_cl[7][3][2][seg_ch_rpc],
        ap_uint<7>      &rpc_link_vl,
        irpc_cluster_t  irpc_cl[2][4][seg_ch_irpc],
        me0_lct_t       me0_lcts[5][seg_ch_me0]
){

    static std::mt19937_64 gen64(0);

    // ******** Simplified ME Method - doesn't worry about invalid values, just send them in anyways
    for(int st=0; st<6; st++){
        for(int ch=0; ch<9; ch++){

            int num_segs = get_num_segs();
            int max_hs = 100;
            int max_wg = 20;

            for(int seg=0; seg<num_segs; seg++){
                csc_lcts[st][ch][seg].hs = gen64() % max_hs;
                csc_lcts[st][ch][seg].wg = gen64() % max_wg;
                csc_lcts[st][ch][seg].vf = 1;
                csc_lcts[st][ch][seg].cp = 8;
                csc_lcts[st][ch][seg].ql = 6;
                // qses? qual?

                //std::cout << "[0," << st << "," << ch << "," << seg << ",0,0," << csc_lcts[st][ch][seg].hs << "," << csc_lcts[st][ch][seg].wg << "]," << std::endl;
            }
        }
    }

    // GE11
    for(int ch=0; ch<7; ch++){
        int num_segs = get_num_segs();
        int max_pad = 192;
        int max_roll = 8;
        for(int seg=0; seg<num_segs; seg++){
            ge11_cl[ch][0][seg].pad = gen64() % max_pad;
            ge11_cl[ch][0][seg].roll = gen64() % max_roll;
            ge11_cl[ch][0][seg].cl_sz = 2;
            ge11_cl[ch][0][seg].valid = 1;

            //std::cout << "[1,0," << ch << "," << seg << ",0,0," <<  ge11_cl[ch][0][seg].pad << ","  << ge11_cl[ch][0][seg].roll << "]," << std::endl;

            ge11_cl[ch][1][seg].pad = gen64() % max_pad;
            ge11_cl[ch][1][seg].roll = gen64() % max_roll;
            ge11_cl[ch][1][seg].cl_sz = 2;
            ge11_cl[ch][1][seg].valid = 1;


            //std::cout << "[1,0," << ch << "," << seg << ",1,0," <<  ge11_cl[ch][1][seg].pad << ","  << ge11_cl[ch][1][seg].roll << "]," << std::endl;
        }
    }

    // GE21
    for(int ch=0; ch<4; ch++){
        int num_segs = get_num_segs();
        int max_pad = 384;
        int max_roll = 8;
        for(int seg=0; seg<num_segs; seg++){
            ge21_cl[ch][0][seg].pad = gen64() % max_pad;
            ge21_cl[ch][0][seg].roll = gen64() % max_roll;
            ge21_cl[ch][0][seg].cl_sz = 2;
            ge21_cl[ch][0][seg].valid = 1;

            //std::cout << "[2,1," << ch << "," << seg << ",0,0," <<  ge21_cl[ch][0][seg].pad << ","  << ge21_cl[ch][0][seg].roll << "]," << std::endl;

            ge21_cl[ch][1][seg].pad = gen64() % max_pad;
            ge21_cl[ch][1][seg].roll = gen64() % max_roll;
            ge21_cl[ch][1][seg].cl_sz = 2;
            ge21_cl[ch][1][seg].valid = 1;

            //std::cout << "[2,1," << ch << "," << seg << ",1,0," <<  ge21_cl[ch][1][seg].pad << ","  << ge21_cl[ch][1][seg].roll << "]," << std::endl;
        }
    }

    // ME0
    for(int ch=0; ch<5; ch++){
        int num_segs = get_num_segs();
        int max_hs = 768;
        int max_roll = 16;
        for(int seg=0; seg<num_segs; seg++){
            me0_lcts[ch][seg].halfstrip = gen64() % max_hs;
            me0_lcts[ch][seg].roll = gen64() % max_roll;
            me0_lcts[ch][seg].bend = 22;
            me0_lcts[ch][seg].qual = 4;
            me0_lcts[ch][seg].valid = 1;

            //std::cout << "[5,0," << ch << "," << seg << ",0,0," << me0_lcts[ch][seg].halfstrip << ","  << me0_lcts[ch][seg].roll << "]," << std::endl;
        }
    }

    // RPC
    const int rpc_theta_ranges[3][2][2] = {
        { {52,84}, {52,88}}, // RE12 and RE22
        { {40,65}, {65,84}}, // RE32 and RE33 - 65 was an arbitrary choice
        { {35,65}, {65,84}}  // RE42 and RE43 - 65 was an arbitrary choice
    };


    const int chamber_ph_init_10deg[7]  = {38, 75, 113, 150, 188, 225, 263}; // These are inclusive
    const int chamber_ph_cover_10deg[7] = {90, 127, 165, 202, 240, 277, 315}; // These are not inclusive (90 means <90)
     
    for(int link=0; link<7; link++){
        for(int i=0; i<3; i++){
            for(int j=0; j<2; j++){
                int num_segs = get_num_segs();
                for(int seg=0; seg<num_segs; seg++){
                    // Phi
                    unsigned ch_start_col = chamber_ph_init_10deg[link]; // each link represents a 10 degree slice across all RPCs
                    unsigned ch_start_phi = ch_start_col << 2; // at rpc phi resolution (11 bits)
                    int ch_width = 52; // max coverage (in cols) for 10 degree chamber
                    rpc_cl[link][i][j][seg].phi = (ch_start_phi + (gen64() % (((ch_width-1) << 2) + 3))); // phi value can cover whole chamber range

                    // Theta
                    unsigned th_min = rpc_theta_ranges[i][j][0];
                    unsigned th_max = rpc_theta_ranges[i][j][1];
                    rpc_cl[link][i][j][seg].theta = th_min + (gen64() % (th_max-th_min));

                    // Other
                    rpc_cl[link][i][j][seg].rpc_time = th_min + (gen64() % (th_max-th_min));
                    rpc_link_vl[link] = 1; // This link needs to be valid for segment to be valid
                    
                    // verify that it is within range
                    assert( (rpc_cl[link][i][j][seg].phi >> 2) >= ch_start_col && (rpc_cl[link][i][j][seg].phi >> 2) < chamber_ph_cover_10deg[link]);
                }
            }
        }
    }

    
    // iRPC
    const unsigned int chamber_ph_init_20deg[4]       = {0, 75, 150, 225};
    const unsigned int chamber_ph_cover_20deg[4]      = {90, 165, 240, 315};

    const unsigned int irpc_theta_ranges[2][2] = {
        {4,  36}, // RE31
        {4,  31} // RE41
    };

    for(int st=0; st<2; st++){
        for(int ch=0; ch<4; ch++){
            int num_segs = get_num_segs();
            for(int seg=0; seg<num_segs; seg++){
                // Phi
                unsigned ch_start_col = chamber_ph_init_20deg[ch]; // each link represents a 10 degree slice across all RPCs
                unsigned ch_start_phi = ch_start_col << 2; // at irpc phi resolution (11 bits)
                int ch_width = 90; // max coverage (in cols) for 20 degree chamber
                irpc_cl[st][ch][seg].phi = (ch_start_phi + (gen64() % (((ch_width-1) << 2) + 3))); // phi value can cover whole chamber range
                    
                // Theta
                unsigned th_min = irpc_theta_ranges[st][0] << 3; // 11 bit theta
                unsigned th_max = irpc_theta_ranges[st][1] << 3;
                //irpc_cl[st][ch][seg].theta = th_min + (gen64() % (th_max-th_min));
                irpc_cl[st][ch][seg].theta = 5 << 3; // force all hits to theta=5   

                // Other
                irpc_cl[st][ch][seg].valid = 1;
                irpc_cl[st][ch][seg].irpc_time = 5;
                
                // verify that it is within range
                assert( (irpc_cl[st][ch][seg].phi >> 2) >= ch_start_col && (irpc_cl[st][ch][seg].phi >> 2) < chamber_ph_cover_20deg[ch]);
                assert( (irpc_cl[st][ch][seg].theta >> 3) >= irpc_theta_ranges[st][0] && (irpc_cl[st][ch][seg].theta >> 3) < irpc_theta_ranges[st][1]);
            }
        }
    }

}



#endif
