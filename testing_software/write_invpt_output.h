#ifndef __PROMPT_TRKBUILD_WRITE_INVPT_OUTPUT_EVENT_H__
#define __PROMPT_TRKBUILD_WRITE_INVPT_OUTPUT_EVENT_H__

#include <random>
#include "../emtf_core/common.h"
#include "../emtf_core/types.h"

void write_invpt_output(const int ievent, trk_invpt_t invpts[num_tracks]){


    std::stringstream filename;
    filename << "emtf_tb_data/emtf_duped_invpt_data_out/result_" << ievent << ".txt";

    std::ofstream myfile; 
    myfile.open(filename.str());

    if ( myfile.is_open() ) { 
        for(int trk=0; trk<num_tracks; trk++){
            myfile << invpts[trk];
            if(trk != num_tracks-1)
                myfile << ", ";
        }
        
        myfile.close();
    }else{
        std::cout << "Failed to open load file." << std::endl;
    }
}


#endif
