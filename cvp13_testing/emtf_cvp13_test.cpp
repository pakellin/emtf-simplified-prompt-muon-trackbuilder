#include <chrono>
#include "../emtf_core/sp.h"
#include "../testing_software/random_tp_generator.h"

// Hardware Interface Files
#include "rw_reg.h"
#include "test_functions.h"


std::vector<std::vector<unsigned int>> store_event(
        csc_lct_t       csc_lcts[6][9][seg_ch_csc],
        gem_cluster_t   ge11_cl[7][gem_layers][seg_ch_gem],
        gem_cluster_t   ge21_cl[4][gem_layers][seg_ch_gem],
        rpc_cluster_t   rpc_cl[7][3][2][seg_ch_rpc],
        ap_uint<7>      rpc_link_vl,
        irpc_cluster_t  irpc_cl[2][4][seg_ch_irpc],
        me0_lct_t       me0_lcts[5][seg_ch_me0] ){

    std::vector<std::vector<unsigned int>> single_event;

    // Store cscs
    for(unsigned int st=0; st<6; st++)
        for(unsigned int ch=0; ch<9; ch++)
            for(unsigned int seg=0; seg<seg_ch_csc; seg++)
                if(csc_lcts[st][ch][seg].vf == 1){
                    std::vector<unsigned int> csc_hit{0,st,ch,seg,0,0,csc_lcts[st][ch][seg].hs,csc_lcts[st][ch][seg].wg};
                    single_event.push_back(csc_hit);
                }

    // Store GEMs
    for(unsigned int ch=0; ch<7; ch++)
        for(unsigned int layer=0; layer<gem_layers; layer++)
            for(unsigned int seg=0; seg<seg_ch_gem; seg++)
                if(ge11_cl[ch][layer][seg].valid == 1){
                    std::vector<unsigned int> ge11_hit{1,0,ch,seg,layer,0,ge11_cl[ch][layer][seg].pad, ge11_cl[ch][layer][seg].roll}; 
                    single_event.push_back(ge11_hit);
                }

    for(unsigned int ch=0; ch<4; ch++)
        for(unsigned int layer=0; layer<gem_layers; layer++)
            for(unsigned int seg=0; seg<seg_ch_gem; seg++)
                if(ge21_cl[ch][layer][seg].valid == 1){
                    std::vector<unsigned int> ge21_hit{2,0,ch,seg,layer,0,ge21_cl[ch][layer][seg].pad, ge21_cl[ch][layer][seg].roll}; 
                    single_event.push_back(ge21_hit);
                }

    // Store RPCs
    for(unsigned int link=0; link<7; link++)
        for(unsigned int i=0; i<3; i++)
            for(unsigned int j=0; j<2; j++)
                for(unsigned int seg=0; seg<seg_ch_rpc; seg++)
                    if(rpc_cl[link][i][j][seg].phi != 0 || rpc_cl[link][i][j][seg].theta != 0){
                        // valid if all phi and theta bits are not 0 (different in fw)
                        std::vector<unsigned int> rpc_hit{3,i,j,seg,0,link,rpc_cl[link][i][j][seg].phi , rpc_cl[link][i][j][seg].theta}; 
                        single_event.push_back(rpc_hit);
                    }

    // RPC Links
    std::vector<unsigned int> rpc_links_vl_vector{6,0,0,0,0,0,unsigned(rpc_link_vl)};
    single_event.push_back(rpc_links_vl_vector);

    // Store iRPCs
    for(unsigned int st=0; st<2; st++)
        for(unsigned int ch=0; ch<4; ch++)
            for(unsigned int seg=0; seg<seg_ch_rpc; seg++)
                if(irpc_cl[st][ch][seg].valid == 1){
                    std::vector<unsigned int> irpc_hit{4,st,ch,seg,0,0,irpc_cl[st][ch][seg].phi , irpc_cl[st][ch][seg].theta}; 
                    single_event.push_back(irpc_hit);
                }


    // Store ME0
    for(unsigned int ch=0; ch<5; ch++)
        for(unsigned int seg=0; seg<seg_ch_me0; seg++)
            if(me0_lcts[ch][seg].valid == 1){
                std::vector<unsigned int> me0_hit{5,0,ch,seg,0,0,me0_lcts[ch][seg].halfstrip, me0_lcts[ch][seg].roll};
                single_event.push_back(me0_hit);
            }

    return single_event;
}

void print_emulator_results( 
        trk_feat_t trk_features_out[num_tracks][num_emtf_features], 
        trk_feat_t trk_features_rm_out[num_tracks][num_emtf_features], 
        trk_invpt_t trk_invpts_out[num_tracks]){

    std::cout << "\n\nEmulator Output\n\n";
    for(int trk=0; trk<4; trk++){
        for(int feat=0; feat<40; feat++)
            std::cout << trk_features_rm_out[trk][feat] << ", ";
        std::cout << "\n";
    }
    std::cout << "\n";

    // Just to output something
    for(int trk=0; trk<4; trk++)
        std::cout << trk_invpts_out[trk] << ", ";
    std::cout << "\n\n";
}


int main(int argc, char** argv) {
    // Setup Hardware Interface
    char find_cvp13[] = "auto";
    rwreg_init(find_cvp13, 0);
    std::cout << "Board Id: " << std::hex << rReg(board_id_addr) << std::dec << std::endl;

    const int n_epochs = 10;
    const int n_events = 4000; // max of ~4000 per epoch
    if(n_events > 4000){ 
        std::cout << "Unsupported number of events.\n";
        return -1;
    }

    int endcap = 1;
    int sector = 1;

    for(int epoch=0; epoch<n_epochs; epoch++){
        std::cout << "\n\nEpoch " << epoch << std::endl;
        // Store Hardware Event Inputs
        std::vector<std::vector<std::vector<unsigned int>>> events;

        // For Storing Emu Event Inputs
        csc_lct_t       csc_lcts[n_events][6][9][seg_ch_csc];
        gem_cluster_t   ge11_cl[n_events][7][gem_layers][seg_ch_gem];
        gem_cluster_t   ge21_cl[n_events][4][gem_layers][seg_ch_gem];
        rpc_cluster_t   rpc_cl[n_events][7][3][2][seg_ch_rpc];
        ap_uint<7>      rpc_link_vl[n_events];
        irpc_cluster_t  irpc_cl[n_events][2][4][seg_ch_irpc];
        me0_lct_t       me0_lcts[n_events][5][seg_ch_me0];

        // Zero Everything
        std::memset(csc_lcts, 0, sizeof csc_lcts);
        std::memset(ge11_cl, 0, sizeof ge11_cl);
        std::memset(ge21_cl, 0, sizeof ge21_cl);
        std::memset(rpc_cl, 0, sizeof rpc_cl);          // Note: in fw, all bits set to 1 means invalid
        std::memset(rpc_link_vl, 0, sizeof rpc_link_vl);
        std::memset(irpc_cl, 0, sizeof irpc_cl);
        std::memset(me0_lcts, 0, sizeof me0_lcts);


        // Generate event inputs
        std::cout << "\nGenerating TPs for Events\n";
        for(int evt=0; evt<n_events; evt++){
            // Create random events 
            random_segment_generator( csc_lcts[evt], ge11_cl[evt], ge21_cl[evt], rpc_cl[evt], rpc_link_vl[evt], irpc_cl[evt], me0_lcts[evt]);

            // Store the inputs for hardware test
            std::vector<std::vector<unsigned int>> single_event = store_event( csc_lcts[evt], ge11_cl[evt], ge21_cl[evt], rpc_cl[evt], rpc_link_vl[evt], irpc_cl[evt], me0_lcts[evt]);
            events.push_back(single_event);
        }



        /******* Testing The Events *********/
        int num_mismatches = 0;
                
        // Run Hardware test on events
        return_to_loading_state();
        std::cout << "Loading Events into FPGA\n";
        load_emtf_event(events);
        start_running_test();


        // Compare hardware Outputs with Emualator
        for(int evt=0; evt<n_events; evt++){
            
            if(evt % 1000 == 0)
                std::cout << "\nTesting Event " << evt;

            // Single Event Outputs
            trk_feat_t trk_features_out[num_tracks][num_emtf_features];
            trk_feat_t trk_features_rm_out[num_tracks][num_emtf_features];
            trk_invpt_t trk_invpts_out[num_tracks];
            
            // Run Sector Processor
            sector_processor(endcap, sector, csc_lcts[evt], ge11_cl[evt], ge21_cl[evt], rpc_cl[evt], rpc_link_vl[evt], irpc_cl[evt], me0_lcts[evt], 
                             trk_features_out, trk_features_rm_out, trk_invpts_out);
            
            //print_emulator_results(trk_features_out, trk_features_rm_out, trk_invpts_out);
            
            // Read features
            std::vector<std::vector<std::vector<int>>> feat_results;
            std::vector<std::vector<int>> prompt_invpt_results;
            std::vector<std::vector<int>> disp_NN_results;

            int read_n_evts = 1;
            feat_results = read_all_trk_features(read_n_evts,19+evt);
            prompt_invpt_results = read_prompt_invpt(read_n_evts,24+evt);
            disp_NN_results = read_disp_NN(read_n_evts,24+evt);

            bool mismatch = false;
            for(int trk=0; trk<num_tracks; trk++){
                for(int feat=0; feat<num_emtf_features; feat++){
                    if((trk_features_rm_out[trk][feat] != feat_results.at(0).at(trk).at(feat)) || (trk_invpts_out[trk] != prompt_invpt_results.at(0).at(trk))){
                        // There was an output mismatch
                        mismatch = true;
                    }
                }
            }

            if(mismatch){
                std::cout << "\nMismatch, Event " << evt << "\n\n";

                for(auto hit : events[evt]){
                    for(auto val : hit)
                        std::cout << val << ", ";
                    std::cout << std::endl;
                }

                print_emulator_results(trk_features_out, trk_features_rm_out, trk_invpts_out);
                print_all_outputs(read_n_evts, feat_results, prompt_invpt_results, disp_NN_results);
                num_mismatches++;
            }

            // Debugging
            //read_me11_lcts(read_n_evts, 5+evt);
            //read_me11_segs(read_n_evts, 9+evt);
            //print_all_outputs(read_n_evts, feat_results, prompt_invpt_results, disp_NN_results);
        }

        std::cout << "\n\nTested " << n_events << " Events - " << num_mismatches << " mismatches.\n\n";

        return_to_loading_state();
    }

    // Close FPGA pci resource file
    rwreg_close();


    return 0;
}

