#ifndef __ADDRESSES_H__
#define __ADDRESSES_H__

// Setup Addresses (barely any for now so don't really need an address table)

// Board ID Address
unsigned board_id_addr = 0x2;

// EMTF module base address
unsigned emtf_base_addr = 0x900000;

// local addresses
unsigned addri_loc_addr = 0x000;   
unsigned valuei_loc_addr = 0x002;
unsigned wei_loc_addr 	= 0x003;
unsigned read_loc_addr 	= 0x008;
unsigned pc_lut_in1_loc_addr = 0x9;
unsigned pc_lut_in2_loc_addr = 0xa;
unsigned pc_lut_out_loc_addr = 0xb;

// Global Addresses (shifted by 2 for word addresses)
unsigned addri_addr = (emtf_base_addr + addri_loc_addr);
unsigned valuei_addr = (emtf_base_addr + valuei_loc_addr);
unsigned wei_addr = (emtf_base_addr + wei_loc_addr);
unsigned emtf_output_addr= (emtf_base_addr + read_loc_addr);
unsigned pc_lut_in1_addr = (emtf_base_addr + pc_lut_in1_loc_addr);
unsigned pc_lut_in2_addr = (emtf_base_addr + pc_lut_in2_loc_addr);
unsigned pc_lut_out_addr = (emtf_base_addr + pc_lut_out_loc_addr);

#endif
