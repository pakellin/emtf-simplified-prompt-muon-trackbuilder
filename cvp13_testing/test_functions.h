#ifndef __CVP13_TEST_FUNCTIONS_H__
#define __CVP13_TEST_FUNCTIONS_H__

#include <iostream>
#include <vector>
#include <unistd.h>

#include "../emtf_core/common.h"
#include "addresses.h"
#include "rw_reg.h"


unsigned int pack_csc_lcts(int vf,int hs,int wg,int qual=6,int cp=8,int qses=0){
    return ((vf << 25) | (hs << 17) | (wg << 10) | (qual << 6) | (cp << 2) | (qses));
}

unsigned int pack_gem(int valid, int pad, int roll, int cl_sz=2){
    return ((valid << 15) | (pad << 6) | (roll << 3) | (cl_sz));
}

unsigned int pack_rpc(int phi, int theta, int time=5){
    return ((phi << 10) | (theta << 4) | (time));
}

unsigned int pack_irpc(int valid, int phi, int theta, int time=5){
    return ((phi << 16) | (theta << 5) | (time << 1) | valid);
}

unsigned int pack_me0(int valid, int halfstrip, int roll, int bend=22, int qual=4){
    return ((valid << 27) | (halfstrip << 17) | (roll << 13) | (bend << 4) | (qual));
}

void load_emtf_event(std::vector<std::vector< std::vector<unsigned int>>> events, int event_idx=0){
    for(auto event: events){
        for(auto hit : event){
            unsigned int tp_type = hit.at(0); // csc-0, ge11-1, ge21-2, rpc-3, irpc-4, me0-5, rpc_link-6
            unsigned int station = hit.at(1);
            unsigned int chamber = hit.at(2);
            unsigned int segment = hit.at(3);
            unsigned int layer_num = hit.at(4); // gem only
            unsigned int link_num = hit.at(5); // rpc only

            // Address 
            unsigned int addri_val = (((link_num | layer_num) << 14) | (segment << 11) | (chamber << 7) | (station << 4) | (tp_type));

            // Value (info must be packed based on tp_type format)
            unsigned int valuei_val;
            if(tp_type == 0)
                valuei_val = pack_csc_lcts(1, hit.at(6), hit.at(7));
            else if(tp_type == 1 || tp_type == 2)
                valuei_val = pack_gem(1, hit.at(6), hit.at(7));
            else if(tp_type == 3)
                valuei_val = pack_rpc(hit.at(6), hit.at(7));
            else if(tp_type == 4)
                valuei_val = pack_irpc(1, hit.at(6), hit.at(7));
            else if(tp_type == 5)
                valuei_val = pack_me0(1, hit.at(6), hit.at(7));
            else if(tp_type == 6)
                valuei_val = hit.at(6);

            wReg(addri_addr, addri_val);
            wReg(valuei_addr, valuei_val);
            wReg(wei_addr,0x1); //pulse
        }

        unsigned int code = 20; // code to increment to next event number
        unsigned int addri_val = (code << 27);
        wReg(addri_addr, addri_val);
        wReg(wei_addr,0x1); //pulse
    }

}

void start_running_test(){
    unsigned int code = 21; // code to start test
    unsigned int addri_val = (code << 27);
    wReg(addri_addr, addri_val);
    wReg(wei_addr,0x1); //pulse
    std::cout << "Running Test\n";

    float delay_seconds = 0.001;
    sleep(delay_seconds);
}

void return_to_loading_state(){
    // Used to return to loading state. Can be run first incase last run failed and state was never returned
    // Clear value register - done bc if in loading state already, it will accidentally load a segment
    wReg(valuei_addr, 0);

    unsigned int code = 22; // code to go back to load inputs state (need to 0 values again for new test)
    unsigned int addri_val = (code << 27);
    wReg(addri_addr, addri_val);
    wReg(wei_addr,0x1); //pulse
}


std::vector<std::vector<std::vector<int>>> read_all_trk_features(int n_events, int start=17){
    std::vector<std::vector<std::vector<int>>> results;
    unsigned int trk_trait = 0; // 0-features

    // Clear Address, otherwise first read has problems (unclear why)
    wReg(addri_addr, 0);

    // Read Features
    for(int evt=start; evt<(start+n_events); evt++){ 
        std::vector<std::vector<int>> evt_results;
        if(n_events != 1)
            std::cout << "Reading Event " << (evt-start) << "\r";

        for(int trk_num=0; trk_num<num_tracks; trk_num++){ 
            std::vector<int> trk_results;
            for(int feature=0; feature<num_emtf_features; feature++){

                // Get feature for this evt and track
                unsigned int addri_val = (evt << 11 | trk_trait << 9 | feature << 3 | trk_num);
                wReg(addri_addr, addri_val);
                int result = rReg(emtf_output_addr);

                // sign extend from sign bit ([12] is the sign bit)
                if(result & 0x1000)
                    result = (0xfffff000 | result);  //negative, add sign bits up to 32 bits

                trk_results.push_back(result);
            }
            evt_results.push_back(trk_results);
        }
        results.push_back(evt_results);
    } // evts
    return results;
}


std::vector<std::vector<int>> read_prompt_invpt(int n_events, int start=22){
    std::vector<std::vector<int>> results;

    unsigned int trk_trait = 2; //NN Outputs
    unsigned int feature = 3; //prompt invpt

    for(int evt=start; evt<(start+n_events); evt++){ 
        std::vector<int> evt_results;
        if(n_events != 1)
            std::cout << "Reading Prompt Invpt, Event " << (evt-start) << "\r";
        for(int trk_num=0; trk_num<num_tracks; trk_num++){ 
            unsigned int addri_val = (evt << 11 | trk_trait << 9 | feature << 3 | trk_num);
            wReg(addri_addr, addri_val);

            int result = rReg(emtf_output_addr);
            // sign extend from sign bit (bit [13])
            if(result & 0x2000)
                result = (0xffffe000 | result); // negative, add sign bits up to 32 bits

            evt_results.push_back(result); 
        }
        results.push_back(evt_results);
    }
    return results;
}


std::vector<std::vector<int>> read_disp_NN(int n_events, int start=22){
    std::vector<std::vector<int>> results;

    unsigned int trk_trait = 2; //NN Outputs

    for(int evt=start; evt<(start+n_events); evt++){ 
        std::vector<int> evt_results;
        if(n_events != 1)
            std::cout << "Reading Disp NN, Event " << (evt-start) << "\r";

        for(int trk_num=0; trk_num<num_tracks; trk_num++){ 
            for(int feat_slc=1; feat_slc<=2; feat_slc++){ // 1 is invpt, 2 is displacement
                unsigned int addri_val = (evt << 11 | trk_trait << 9 | feat_slc << 3 | trk_num);
                wReg(addri_addr, addri_val);

                int result = rReg(emtf_output_addr);
                // sign extend from sign bit (bit [13])
                if(result & 0x2000)
                    result = (0xffffe000 | result); // negative, add sign bits up to 32 bits

                evt_results.push_back(result); 
            }
        }
        results.push_back(evt_results);
    }
    return results;
}


void read_me11_lcts(int n_events, int start=4){
    unsigned int trk_trait = 3; //emtf segs 
    unsigned int segs_or_lcts = 1;
    
    for(int evt=start; evt<start+n_events; evt++){ 
        std::cout << "\n\nEvent " << (evt - start) << std::endl;
        for(int seg=0; seg<14; seg++){ 
            std::cout << "\nSeg: ";
            unsigned int addri_val = (evt << 11 | segs_or_lcts << 7 | trk_trait << 9 | seg);
            wReg(addri_addr, addri_val);
            int result = rReg(emtf_output_addr);

            std::cout << ((result & (1 << 25)) >> 25) << ", ";
            std::cout << ((result & (0xff << 17)) >> 17) << ", ";
            std::cout << ((result & (0x7f << 10)) >> 10) << ", ";
            std::cout << ((result & (0xf << 6)) >> 6) << ", ";
            std::cout << ((result & (0xf << 2)) >> 2) << std::endl;

            //((vf << 25) | (hs << 17) | (wg << 10) | (qual << 6) | (cp << 2) | (qses))
        }
    }
}

void read_me11_segs(int n_events, int start=9){
    unsigned int trk_trait = 3; //emtf segs 
    
    for(int evt=start; evt<start+n_events; evt++){ 
        std::cout << "\n\nEvent " << (evt - start) << std::endl;
        for(int seg=0; seg<14; seg++){ 
            std::cout << "\nSeg: ";
            for(int feat=0; feat<5; feat++){ 
                unsigned int addri_val = (evt << 11 | trk_trait << 9 | feat << 4 | seg);
                wReg(addri_addr, addri_val);
                int result = rReg(emtf_output_addr);
                std::cout << result << ", ";
            }
            std::cout << std::endl;
        }
    }
}

void print_all_outputs(int n_events, std::vector<std::vector<std::vector<int>>> feats, std::vector<std::vector<int>> prompt_invpts, std::vector<std::vector<int>> disp_NN){
    for(int ievt=0; ievt<n_events; ievt++){

        std::cout << "\nTrack Features\n";
        for(auto trk : feats.at(ievt)){
            for(auto feat : trk){
                std::cout << feat << ", ";
            }
            std::cout << std::endl;
        }
        std::cout << "\n\n";

        std::cout << "Prompt Invpts\n";
        for(auto feat : prompt_invpts.at(ievt)){
            std::cout << feat << ", ";
        }
        std::cout << "\n\n";

        std::cout << "Disp NN Outputs\n";
        for(auto feat : disp_NN.at(ievt)){
            std::cout << feat << ", ";
        }
        std::cout << "\n\n";
    }

}

#endif
