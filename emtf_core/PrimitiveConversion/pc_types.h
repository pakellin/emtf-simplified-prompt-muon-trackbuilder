#ifndef __PRIM_CONV_TYPES_H__
#define __PRIM_CONV_TYPES_H__


typedef struct {
        ap_uint<1>       vf;   // valid flag
        ap_uint<8> hs;   // halfstrip
        ap_uint<7> wg;   // wiregroup
        ap_uint<4> ql;   // quality
        ap_uint<4> cp;   // CLCT pattern
        ap_uint<2> qses;
} csc_lct_t;


typedef struct {
        ap_uint<1> valid;
        ap_uint<9> pad;    // use ge21 bw
        ap_uint<3> roll;
        ap_uint<3> cl_sz;
} gem_cluster_t;


typedef struct {
    ap_uint<11> phi;    // Extended to 13 bits
    ap_uint<6> theta;   // Extended to 8 bits
    ap_uint<4> rpc_time;
} rpc_cluster_t;


typedef struct {
    ap_uint<11> phi;    // Extended to 13 bits
    ap_uint<11> theta;  // Reduced to 8 bits
    ap_uint<4>  irpc_time;
    ap_uint<1>  valid;
} irpc_cluster_t;


typedef struct {
    ap_uint<1> valid;
    ap_uint<10> halfstrip;
    ap_uint<4> roll;
    ap_int<9> bend;
    ap_uint<4> qual;
} me0_lct_t;


#endif  
