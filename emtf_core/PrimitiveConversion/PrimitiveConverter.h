#ifndef __PRIMITIVE_CONVERTER_H__
#define __PRIMITIVE_CONVERTER_H__

#include <iostream>
#include <cmath>
#include <vector>
#include <cassert>
#include "../common.h"
#include "../types.h"
#include "pc_params.h"
#include "pc_types.h"

    
// Create the Phi and Theta lookup tables
//                                          { ME0, GE11, ME11, ME12, RE12, GE21, RE22, ME21, ME22, ME31, ME32, RE31, RE32, ME41, ME42, RE41, RE42,   ME13} sites;
const unsigned site_phi_addr_bw[num_sites] = { 13,  12,   13,   11,    0,   12,    0,   12,   11,   12,   11,    0,    0,   12,   11,    0,    0};
const unsigned site_theta_addr_bw[num_sites]={  4,   3,   12,    6,    0,    3,    0,    7,    6,    7,    6,    0,    0,    7,    6,    0,    0}; // ME13=5


void fill_csc_segs(const csc_lct_t csc_lcts[6][9][seg_ch_csc], emtf_seg_t emtf_segs_o[num_sites][max_ch_site][max_seg_ch],
                    std::vector<int> phi_LUTs[num_sites][max_ch_site], std::vector<int> theta_LUTs[num_sites][max_ch_site]){
    // CSC to Site Chamber Mapping
    const unsigned ME13 = 17;
    const unsigned map_cscs_to_site_ch[6][9][2] = {                                         // {Site, Ch_num}
        { {ME11,1}, {ME11,2}, {ME11,3}, {ME12,1}, {ME12,2}, {ME12,3}, {ME13,1}, {ME13,2}, {ME13,3}}, // st0
        { {ME11,4}, {ME11,5}, {ME11,6}, {ME12,4}, {ME12,5}, {ME12,6}, {ME13,4}, {ME13,5}, {ME13,6}}, // st1
        { {ME21,1}, {ME21,2}, {ME21,3}, {ME22,1}, {ME22,2}, {ME22,3}, {ME22,4}, {ME22,5}, {ME22,6}}, // st2
        { {ME31,1}, {ME31,2}, {ME31,3}, {ME32,1}, {ME32,2}, {ME32,3}, {ME32,4}, {ME32,5}, {ME32,6}}, // st3
        { {ME41,1}, {ME41,2}, {ME41,3}, {ME42,1}, {ME42,2}, {ME42,3}, {ME42,4}, {ME42,5}, {ME42,6}}, // st4
        { {ME11,0}, {ME12,0}, {ME13,0}, {ME21,0}, {ME22,0}, {ME31,0}, {ME32,0}, {ME41,0}, {ME42,0}}  // neighbors
    };

    const int map_csc_patt_to_emtf_bend[11]  = {0, 0,-5, 5,-5, 5,-2, 2,-2, 2, 0};

    for(int csc_st=0; csc_st<6; csc_st++){
        for(int csc_ch=0; csc_ch<9; csc_ch++){
            int site = map_cscs_to_site_ch[csc_st][csc_ch][0];
            int ch = map_cscs_to_site_ch[csc_st][csc_ch][1];

            // Skip ME13 for now
            if(site == ME13)
                continue;
            
            for(int seg=0; seg<seg_ch_csc; seg++){
                ap_uint<10> eighth_strip;
                unsigned phi_addr; // Actual bit-width varies by site
                unsigned theta1_addr;
                unsigned theta2_addr;
                csc_lct_t curr_lct = csc_lcts[csc_st][csc_ch][seg];
                csc_lct_t alt_lct = csc_lcts[csc_st][csc_ch][(seg==0)? 1 : 0];
                int emtf_bend = (curr_lct.cp < 11)? map_csc_patt_to_emtf_bend[curr_lct.cp] : 0;

                if(site == ME11){
                    // ME11, use qses bits, use 3wg bits for phi address, and special handling of theta address to deal with tilted wires
                    eighth_strip = (curr_lct.hs, curr_lct.qses);
                    phi_addr = (eighth_strip, curr_lct.wg.range(5,3)); // 3 wg bits (MSB of .wg isn't use for ME11)
                    theta1_addr = (curr_lct.wg.range(5,0), curr_lct.hs.range(7,2)); 
                    theta2_addr = (alt_lct.wg.range(5,0), curr_lct.hs.range(7,2));
                }
                else{
                    if(site == ME21 || site == ME31 || site == ME41){
                        // Ring 1 CSCs, use qses bits and use 2 wg bits to add to phi precision
                        eighth_strip = (curr_lct.hs, curr_lct.qses);
                        phi_addr = (eighth_strip, curr_lct.wg.range(6,5)); // 2 wg bits
                        theta1_addr = curr_lct.wg;
                        theta2_addr = alt_lct.wg;
                    }
                    else{
                        // Ring 2 (TMB) chambers, have to use clct pattern for 1/8th strip
                        bool emtf_bend_sign = (emtf_bend < 0)? 1 : 0;
                        ap_uint<3> emtf_bend_val = std::abs(emtf_bend);

                        eighth_strip = (curr_lct.hs, ap_uint<2>(0)); // qses bits are 0
                        if(emtf_bend_sign == 0)
                            eighth_strip = eighth_strip + emtf_bend_val.range(2,1); // positive bend, add bend offset to 1/8th strip
                        else if(eighth_strip > 0)
                            eighth_strip = eighth_strip - emtf_bend_val.range(2,1); // negative bend

                        phi_addr = (eighth_strip, curr_lct.wg[6]); // 1 wg bit 
                        theta1_addr = curr_lct.wg;
                        theta2_addr = alt_lct.wg;
                    }
                }

                
                emtf_segs_o[site][ch][seg].emtf_phi = (curr_lct.vf == 1)? phi_LUTs[site][ch][phi_addr] : 0;
                emtf_segs_o[site][ch][seg].emtf_theta1 = (curr_lct.vf == 1)? theta_LUTs[site][ch][theta1_addr] : 0;
                emtf_segs_o[site][ch][seg].emtf_theta2 = (alt_lct.vf == 1)? theta_LUTs[site][ch][theta2_addr] : 0; // only valid when other seg is valid
                emtf_segs_o[site][ch][seg].emtf_bend = (curr_lct.vf == 1)? emtf_bend : 0;
                emtf_segs_o[site][ch][seg].emtf_qual = (curr_lct.vf == 1)? 6 : 0;
                emtf_segs_o[site][ch][seg].seg_valid = curr_lct.vf;

                // Not used here
                emtf_segs_o[site][ch][seg].emtf_time = 0;
                emtf_segs_o[site][ch][seg].seg_bx = 0;
                emtf_segs_o[site][ch][seg].seg_zones = 0; // overwrite later
            }        
        } // csc_ch
    } // csc_st
}

void fill_me0_segs(const me0_lct_t me0_lcts[5][seg_ch_me0], emtf_seg_t emtf_segs_o[num_sites][max_ch_site][max_seg_ch],
                    std::vector<int> phi_LUTs[num_sites][max_ch_site], std::vector<int> theta_LUTs[num_sites][max_ch_site]){

    int site = ME0;

    for(int ch=0; ch<5; ch++){
        for(int seg=0; seg<seg_ch_me0; seg++){
            me0_lct_t curr_lct = me0_lcts[ch][seg];

            // ME0 uses halfstrip value and 3 roll bits for phi
            ap_uint<10> eighth_strip = curr_lct.halfstrip;
            unsigned phi_addr = (eighth_strip, curr_lct.roll.range(3,1)); // 3 roll bits 
            unsigned theta1_addr = curr_lct.roll;

            emtf_segs_o[site][ch][seg].emtf_phi = (curr_lct.valid == 1)? phi_LUTs[site][ch][phi_addr] : 0;
            emtf_segs_o[site][ch][seg].emtf_theta1 = (curr_lct.valid == 1)? theta_LUTs[site][ch][theta1_addr] : 0;
            emtf_segs_o[site][ch][seg].emtf_bend = (curr_lct.valid == 1)? static_cast<emtf_bend_t>(curr_lct.bend) : static_cast<emtf_bend_t>(0);
            emtf_segs_o[site][ch][seg].emtf_qual = (curr_lct.valid == 1)? static_cast<emtf_qual_t>(curr_lct.qual) : static_cast<emtf_qual_t>(0);
            emtf_segs_o[site][ch][seg].seg_valid = curr_lct.valid;

            // Not used here
            emtf_segs_o[site][ch][seg].emtf_theta2 = 0;
            emtf_segs_o[site][ch][seg].emtf_time = 0;
            emtf_segs_o[site][ch][seg].seg_bx = 0;
            emtf_segs_o[site][ch][seg].seg_zones = 0; // overwrite later
        }
    }
}

void fill_gem_segs(const gem_cluster_t ge11_cl[7][gem_layers][seg_ch_gem], 
                    const gem_cluster_t ge21_cl[4][gem_layers][seg_ch_gem], 
                    emtf_seg_t emtf_segs_o[num_sites][max_ch_site][max_seg_ch],
                    std::vector<int> phi_LUTs[num_sites][max_ch_site], std::vector<int> theta_LUTs[num_sites][max_ch_site]){

    // GEM Parameters
    int max_delta_pad = 4;
    int max_delta_roll = 1;
    int max_pad = 7;
    int max_strip;

    // For readability
    int layer1 = 0;
    int layer2 = 1;

    bool coincidence[seg_ch_gem];

    for(int st=0; st<2; st++){
        int num_gem_chambers = (st==0)? 7 : 4;
        max_strip = (st==0)? 191 : 383;

        for(int ch=0; ch<num_gem_chambers; ch++){

            // Get current station-chamber cluster
            gem_cluster_t gem_cl[gem_layers][seg_ch_gem]; 

            for(int layer=0; layer<gem_layers; layer++)
                for(int seg=0; seg<seg_ch_gem; seg++)
                    gem_cl[layer][seg] = (st==0)? ge11_cl[ch][layer][seg] : ge21_cl[ch][layer][seg];


            for(int seg=0; seg<seg_ch_gem; seg++){
                coincidence[seg] = 0;

                if(gem_cl[layer1][seg].valid == 1){
                    // Check for coincidence with layer 2 segments
                    for(int seg_l2=0; seg_l2<seg_ch_gem; seg_l2++){
                        if(gem_cl[layer2][seg_l2].valid == 1){
                            // get pad coincidence range (from 4 below lowest pad -> 4 above highest pad (pad + cl_sz + 4))
                            ap_uint<9> pad_range_low = (gem_cl[layer1][seg].pad <= max_delta_pad)? ap_uint<9>(0) : ap_uint<9>(gem_cl[layer1][seg].pad - max_delta_pad);
                            ap_uint<9> pad_range_high = (gem_cl[layer1][seg].pad >= (max_strip - max_delta_pad - gem_cl[layer1][seg].cl_sz))? ap_uint<9>(max_strip) :
                                        ap_uint<9>(gem_cl[layer1][seg].pad + gem_cl[layer1][seg].cl_sz + max_delta_pad);
                            
                            // get roll coincidence range
                            ap_uint<3> roll_range_low = (gem_cl[layer1][seg].roll <= max_delta_roll)? ap_uint<3>(0) : ap_uint<3>(gem_cl[layer1][seg].roll - max_delta_roll);
                            ap_uint<3> roll_range_high = (gem_cl[layer1][seg].roll >= (max_pad - max_delta_roll))? ap_uint<3>(max_pad) : ap_uint<3>(gem_cl[layer1][seg].roll + max_delta_roll);

                            // get layer 2 cluster pad min/max and roll
                            ap_uint<9> l2_pad_min = gem_cl[layer2][seg_l2].pad;
                            ap_uint<9> l2_pad_max = gem_cl[layer2][seg_l2].pad + gem_cl[layer2][seg_l2].cl_sz;
                            ap_uint<3> l2_roll = gem_cl[layer2][seg_l2].roll;

                            // If any pads in the cluster are in range, than there is coincidence
                            if( (pad_range_low <= l2_pad_max) and (l2_pad_min <= pad_range_high) and (roll_range_low <= l2_roll) and (l2_roll <= roll_range_high)){
                                coincidence[seg] |= static_cast<ap_uint<1>>(1);
                            }
                        }
                    }
                }

                if(coincidence[seg] == 1){
                    int site = (st == 0)? GE11 : GE21;
                    gem_cluster_t curr_cl = gem_cl[layer1][seg];

                    int phi_addr;
                    if(site == GE11){
                        ap_uint<9> halfpad = (int(curr_cl.pad) * 2 + curr_cl.cl_sz); // Be careful with overflow
                        phi_addr = (halfpad, curr_cl.roll); // Station 1, Ring 1, use all 3 roll bits
                    }else{
                        ap_uint<10> halfpad = (int(curr_cl.pad) * 2 + curr_cl.cl_sz);
                        phi_addr = (halfpad, curr_cl.roll.range(2,1)); // use 2 roll bits
                    }

                    int th_addr = curr_cl.roll;

                    emtf_segs_o[site][ch][seg].emtf_phi = phi_LUTs[site][ch][phi_addr];
                    emtf_segs_o[site][ch][seg].emtf_theta1 = theta_LUTs[site][ch][th_addr];
                    emtf_segs_o[site][ch][seg].emtf_qual = curr_cl.cl_sz;
                    emtf_segs_o[site][ch][seg].seg_valid = curr_cl.valid;

                    // Not used here
                    emtf_segs_o[site][ch][seg].emtf_bend = 0;
                    emtf_segs_o[site][ch][seg].emtf_theta2 = 0;
                    emtf_segs_o[site][ch][seg].seg_bx = 0;
                    emtf_segs_o[site][ch][seg].emtf_time = 0;
                    emtf_segs_o[site][ch][seg].seg_zones = 0; // overwrite later
                }

            } // seg
        } // ch
    } // st
}

void fill_rpc_segs(const rpc_cluster_t rpc_cl[7][3][2][seg_ch_rpc], const ap_uint<7> rpc_link_vl, emtf_seg_t emtf_segs_o[num_sites][max_ch_site][max_seg_ch]){
    for(int ch=0; ch<7; ch++){
        for(int frame=0; frame<3; frame++){
            for(int site_in_frame=0; site_in_frame<2; site_in_frame++){
                for(int rpc_seg=0; rpc_seg<seg_ch_rpc; rpc_seg++){
                    int site;
                    int seg; 

                    rpc_cluster_t curr_cl = rpc_cl[ch][frame][site_in_frame][rpc_seg];

                    if(frame == 1)
                        site = RE32;
                    else if(frame == 2)
                        site = RE42;
                    else{
                        if(site_in_frame==0)
                            site = RE12;
                        else    
                            site = RE22;
                    }

                    seg = ((site==RE32 || site==RE42) && site_in_frame == 1)? rpc_seg+seg_ch_rpc : rpc_seg; // RE32 & RE42 are rings 2 and 3 merged, so they have 4 segments

                    bool valid = ((rpc_link_vl[ch] == 1) && (curr_cl.phi != 0 || curr_cl.theta != 0)); // Only valid when bits are not all 0 (in fw its all bits are not 1)

                    emtf_segs_o[site][ch][seg].emtf_phi = (valid == 1)? (curr_cl.phi, ap_uint<2>(0)) : static_cast<emtf_phi_t>(0);  // Add 2 bits 11->13
                    emtf_segs_o[site][ch][seg].emtf_theta1 = (valid == 1)? (curr_cl.theta, ap_uint<2>(0)) : static_cast<emtf_theta_t>(0); // Add 2 bits 6->8
                    emtf_segs_o[site][ch][seg].emtf_time = (valid == 1)? static_cast<emtf_time_t>(curr_cl.rpc_time) : static_cast<emtf_time_t>(0);
                    emtf_segs_o[site][ch][seg].seg_valid = valid;

                    // Not used here
                    emtf_segs_o[site][ch][seg].emtf_bend = 0;
                    emtf_segs_o[site][ch][seg].emtf_theta2 = 0;
                    emtf_segs_o[site][ch][seg].seg_bx = 0;
                    emtf_segs_o[site][ch][seg].seg_zones = 0; // overwrite later
                }
            }
        }
    }
    
}

void fill_irpc_segs(const irpc_cluster_t irpc_cl[2][4][seg_ch_irpc], emtf_seg_t emtf_segs_o[num_sites][max_ch_site][max_seg_ch]){
    for(int irpc_st=0; irpc_st<2; irpc_st++){
        for(int ch=0; ch<4; ch++){
            for(int seg=0; seg<seg_ch_irpc; seg++){
                int site = (irpc_st == 0)? RE31 : RE41;
                irpc_cluster_t curr_cl = irpc_cl[irpc_st][ch][seg];

                emtf_segs_o[site][ch][seg].emtf_phi = (curr_cl.valid == 1)? (curr_cl.phi, ap_uint<2>(0)) : static_cast<emtf_phi_t>(0);  // Add 2 bits 11->13
                emtf_segs_o[site][ch][seg].emtf_theta1 = (curr_cl.valid == 1)? curr_cl.theta.range(10,3) : static_cast<emtf_theta_t>(0); // Remove 3 bits 11->8
                emtf_segs_o[site][ch][seg].emtf_time = (curr_cl.valid == 1)? static_cast<emtf_time_t>(curr_cl.irpc_time) : static_cast<emtf_time_t>(0);
                emtf_segs_o[site][ch][seg].seg_valid = curr_cl.valid;

                // Not used here
                emtf_segs_o[site][ch][seg].emtf_bend = 0;
                emtf_segs_o[site][ch][seg].emtf_theta2 = 0;
                emtf_segs_o[site][ch][seg].seg_bx = 0;
                emtf_segs_o[site][ch][seg].seg_zones = 0; // overwrite later
            }
        }
    }
}



void PrimitiveConverter(
            const int endcap, // -1 or 1
            const int sector, // 1..6

            // Trigger Primitive Inputs
            const csc_lct_t       csc_lcts[6][9][seg_ch_csc],
            const gem_cluster_t   ge11_cl[7][gem_layers][seg_ch_gem],
            const gem_cluster_t   ge21_cl[4][gem_layers][seg_ch_gem],
            const rpc_cluster_t   rpc_cl[7][3][2][seg_ch_rpc],
            const ap_uint<7>      rpc_link_vl,
            const irpc_cluster_t  irpc_cl[2][4][seg_ch_irpc],
            const me0_lct_t       me0_lcts[5][seg_ch_me0],

            // EMTF Segment Outputs
            emtf_seg_t emtf_segs_o[num_sites][max_ch_site][max_seg_ch]
){

    std::vector<int> phi_LUTs[num_sites][max_ch_site];
    std::vector<int> theta_LUTs[num_sites][max_ch_site];

    for(int site=0; site<num_sites; site++){
        if(site == RE12 || site == RE22 || site == RE32 || site == RE42 || site == RE31 || site == RE41) // no LUTs for RPCs
            continue;

        for(int ch=0; ch<ch_per_site[site]; ch++){
            int phi_vals_per_ch = std::pow(2,site_phi_addr_bw[site]);
            int theta_vals_per_ch = std::pow(2,site_theta_addr_bw[site]);

            phi_LUTs[site][ch].reserve(phi_vals_per_ch); // create space for current site/ch LUT
            theta_LUTs[site][ch].reserve(theta_vals_per_ch); // create space for current site/ch LUT

            int val;
            char comma;
            
            // Load Phi LUT
            std::stringstream phi_filename; 
            phi_filename << "emtf_core/PrimitiveConversion/PC_LUTs/endcap" << endcap << "/sector" << sector << "/" << site_strings[site] << "_phi_ch" << ch << ".txt";
            std::ifstream phiFile(phi_filename.str());

            assert(phiFile.is_open() == 1);
            while(phiFile >> val >> comma)
                phi_LUTs[site][ch].push_back(val);


            // Load Theta LUT
            std::stringstream theta_filename; 
            theta_filename << "emtf_core/PrimitiveConversion/PC_LUTs/endcap" << endcap << "/sector" << sector << "/" << site_strings[site] << "_theta_ch" << ch << ".txt";
            std::ifstream thetaFile(theta_filename.str());

            assert(thetaFile.is_open() == 1);
            while(thetaFile >> val >> comma)
                theta_LUTs[site][ch].push_back(val);
        }
    }

    // Perform Primitive Conversion Lookups
    fill_csc_segs(csc_lcts, emtf_segs_o, phi_LUTs, theta_LUTs);

    fill_me0_segs(me0_lcts, emtf_segs_o, phi_LUTs, theta_LUTs);

    fill_gem_segs(ge11_cl, ge21_cl, emtf_segs_o, phi_LUTs, theta_LUTs);
    
    fill_irpc_segs(irpc_cl, emtf_segs_o);

    fill_rpc_segs(rpc_cl, rpc_link_vl, emtf_segs_o);


    // Boundary Checks (Invalidate 0 theta or phi segments)
    for(int site=0; site<num_sites; site++)
        for(int ch=0; ch<max_ch_site; ch++)
            for(int seg=0; seg<max_seg_ch; seg++)
                if( (emtf_segs_o[site][ch][seg].seg_valid == 0) ||
                    (emtf_segs_o[site][ch][seg].emtf_phi == 0 || (emtf_segs_o[site][ch][seg].emtf_theta1==0 && emtf_segs_o[site][ch][seg].emtf_theta2==0))){
                        emtf_segs_o[site][ch][seg].seg_valid = 0;
                        emtf_segs_o[site][ch][seg].emtf_phi = 0;
                        emtf_segs_o[site][ch][seg].emtf_theta1 = 0;
                        emtf_segs_o[site][ch][seg].emtf_theta2 = 0;
                }


    
    // Set zone bits (4-26, 23-56, 48-88)
    for(int site=0; site<num_sites; site++){
        for(int ch=0; ch<max_ch_site; ch++){
            for(int seg=0; seg<max_seg_ch; seg++){
                if(emtf_segs_o[site][ch][seg].seg_valid){
                    if(
                        ((emtf_segs_o[site][ch][seg].emtf_theta1 >= 4 && emtf_segs_o[site][ch][seg].emtf_theta1 <= 26) || 
                        (emtf_segs_o[site][ch][seg].emtf_theta2 >= 4 && emtf_segs_o[site][ch][seg].emtf_theta2 <= 26))){
                        emtf_segs_o[site][ch][seg].seg_zones[2] = 1;
                    }

                    if( 
                        ((emtf_segs_o[site][ch][seg].emtf_theta1 >= 23 && emtf_segs_o[site][ch][seg].emtf_theta1 <= 56) || 
                        (emtf_segs_o[site][ch][seg].emtf_theta2 >= 23 && emtf_segs_o[site][ch][seg].emtf_theta2 <= 56))){
                        emtf_segs_o[site][ch][seg].seg_zones[1] = 1;
                    }

                    if( 
                        ((emtf_segs_o[site][ch][seg].emtf_theta1 >= 48 && emtf_segs_o[site][ch][seg].emtf_theta1 <= 88) || 
                        (emtf_segs_o[site][ch][seg].emtf_theta2 >= 48 && emtf_segs_o[site][ch][seg].emtf_theta2 <= 88))){
                        emtf_segs_o[site][ch][seg].seg_zones[0] = 1;
                    }
                }
            }
        }
    }


    // Print Valid Segments
    /*
    std::cout << "Valid Segments:\n";
    for(int site=0; site<num_sites; site++){
        for(int ch=0; ch<max_ch_site; ch++){
            for(int seg=0; seg<max_seg_ch; seg++){
                if(emtf_segs_o[site][ch][seg].seg_valid){
                    emtf_seg_t cseg = emtf_segs_o[site][ch][seg];
                    std::cout << site << "-" << ch << "-" << seg << "\t" << cseg.emtf_phi << ", " << cseg.emtf_theta1 << ", " << 
                                 cseg.emtf_theta2 << ", " << cseg.seg_zones << ", " << cseg.emtf_bend << ", " << cseg.seg_valid << std::endl;
                }
            }
        }
    }
    std::cout << "\n";
    */

}


#endif
