// Top function
#include <ap_int.h>
#include <ap_fixed.h>
#include <iostream>
#include <fstream>

#include "../common.h"
#include "../types.h"
#include "pc_types.h"
#include "PrimitiveConverter.h"
#include "pc_params.h"


// Main driver
int main(int argc, char** argv) {

    // Trigger Primitives
    csc_lct_t       csc_lcts[6][9][seg_ch_csc];
    gem_cluster_t   ge11_cl[7][gem_layers][seg_ch_gem];
    gem_cluster_t   ge21_cl[4][gem_layers][seg_ch_gem];
    rpc_cluster_t   rpc_cl[7][3][2][seg_ch_rpc];
    ap_uint<7>      rpc_link_vl;
    irpc_cluster_t  irpc_cl[2][4][seg_ch_irpc];
    me0_lct_t       me0_lcts[5][seg_ch_me0];


    // Zero all input elements
    std::memset(csc_lcts, 0, sizeof csc_lcts);
    std::memset(ge11_cl, 0, sizeof ge11_cl);
    std::memset(ge21_cl, 0, sizeof ge21_cl);
    std::memset(rpc_cl, 0, sizeof rpc_cl);
    rpc_link_vl = 0;
    std::memset(irpc_cl, 0, sizeof irpc_cl);
    std::memset(me0_lcts, 0, sizeof me0_lcts);

    // EMTF Segments Output
    emtf_seg_t emtf_segs_o[num_sites][max_ch_site][max_seg_ch];

    // Zero outputs  - you don't need to worry about writing all of them in PrimitiveConverter()
    std::memset(emtf_segs_o, 0, sizeof emtf_segs_o);

    /*
    csc_lcts[0][0][0].hs = 202;
    csc_lcts[0][0][0].wg = 10;
    csc_lcts[0][0][0].cp = 8;
    csc_lcts[0][0][0].vf = 1;

    csc_lcts[2][0][0].hs = 48;
    csc_lcts[2][0][0].wg = 29;
    csc_lcts[2][0][0].cp = 10;
    csc_lcts[2][0][0].vf = 1;

    csc_lcts[3][0][0].hs = 118;
    csc_lcts[3][0][0].wg = 34;
    csc_lcts[3][0][0].cp = 10;
    csc_lcts[3][0][0].vf = 1;

    csc_lcts[4][0][0].hs = 122;
    csc_lcts[4][0][0].wg = 35;
    csc_lcts[4][0][0].cp = 10;
    csc_lcts[4][0][0].vf = 1;

    ge11_cl[1][0][0].pad = 27;
    ge11_cl[1][0][0].roll = 7;
    ge11_cl[1][0][0].cl_sz = 0;
    ge11_cl[1][0][0].valid = 1;

    ge11_cl[1][1][0].pad = 28;
    ge11_cl[1][1][0].roll = 7;
    ge11_cl[1][1][0].cl_sz = 0;
    ge11_cl[1][1][0].valid = 1;
    */

    ge21_cl[1][0][1].pad = 376;
    ge21_cl[1][0][1].roll = 0;
    ge21_cl[1][0][1].cl_sz = 2;
    ge21_cl[1][0][1].valid = 1;

    ge21_cl[1][1][0].pad = 380;
    ge21_cl[1][1][0].roll = 0;
    ge21_cl[1][1][0].cl_sz = 2;
    ge21_cl[1][1][0].valid = 1;

    /*
    me0_lcts[1][0].halfstrip = 232;
    me0_lcts[1][0].roll = 2;
    me0_lcts[1][0].bend = 22;
    me0_lcts[1][0].valid = 1;
    */

    int endcap = 1;
    int sector = 1;

    PrimitiveConverter(endcap, sector, csc_lcts, ge11_cl, ge21_cl, rpc_cl, rpc_link_vl, irpc_cl, me0_lcts, emtf_segs_o);

    for(int site=0; site<num_sites; site++){
        for(int ch=0; ch<ch_per_site[site]; ch++){
            for(int seg=0; seg<max_seg_ch; seg++){
                if(emtf_segs_o[site][ch][seg].seg_valid){
                    std::cout << site_strings[site] << "-" << ch << "\t" << emtf_segs_o[site][ch][seg].emtf_phi << " - " << emtf_segs_o[site][ch][seg].emtf_theta1 << "\n";
                }
            }
        }
    }

    return 0;

}
