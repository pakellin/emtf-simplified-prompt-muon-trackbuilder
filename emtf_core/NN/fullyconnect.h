#ifndef __EMTF_HLSLIB_FULLYCONNECT_H__
#define __EMTF_HLSLIB_FULLYCONNECT_H__

// EMTF HLS
#include <cassert>
#include "nnet_weights.h"
#include "nnet_kernels.h"


template <int N, typename T_IN, typename T_OUT, typename T_weights>
void fullyconnect_preprocessing_op(const T_weights weights, const T_IN in0, T_OUT out){
    detail::vec_vec_mult_op<N>(in0, weights, out);
}

template <int N, typename T_IN, typename T_OUT>
void fullyconnect_activation_op(const T_IN in0, T_OUT out) {
    detail::vector_tanh_activate_op<N>(in0, out);
}

template <int M, int N, typename T_weights, typename T_bias, typename T_IN, typename T_OUT>
void fullyconnect_dense_op(const T_weights weights, const T_bias biases, const T_IN in0, T_OUT out){
    detail::mat_vec_mult_biasadd_op<M, N>(in0, weights, biases, out);
}


// _____________________________________________________________________________
// Fully connected

template <typename T_IN, typename T_OUT>
void fullyconnect(const int nn_num, const T_IN* curr_trk_feat_rm, T_OUT* curr_trk_invpt) {

  // Neural Network Model is defined here

    // Intermediate arrays - all NNs must share same size and fixed-point types
    ap_fixed<14,4> layer_0_out[num_nodes[0]];

    // Activation function inputs are 12 bits - 4096 tanh addresses
    ap_fixed<12,3> layer_1_preact[num_nodes[1]];
    ap_fixed<12,3> layer_2_preact[num_nodes[2]];
    ap_fixed<12,3> layer_3_preact[num_nodes[3]];

    // Activation function outputs - 14 bits, 1 integer bit
    ap_fixed<14,1> layer_1_out[num_nodes[1]];
    ap_fixed<14,1> layer_2_out[num_nodes[2]];
    ap_fixed<14,1> layer_3_out[num_nodes[3]];

    ap_fixed<14,1+6> layer_4_out[num_nodes[4]]; // output node is multiplied by 2^6


    // Layer 0 - preprocessing
    fullyconnect_preprocessing_op<num_nodes[0]>(weights_layer_0[nn_num], curr_trk_feat_rm, layer_0_out);

    // Layer 1 - dense + activation
    fullyconnect_dense_op<num_nodes[0], num_nodes[1]>(weights_layer_1[nn_num], biases_layer_1[nn_num], layer_0_out, layer_1_preact);
    fullyconnect_activation_op<num_nodes[1]>(layer_1_preact, layer_1_out);

    // Layer 2 - dense + activation
    fullyconnect_dense_op<num_nodes[1], num_nodes[2]>(weights_layer_2[nn_num], biases_layer_2[nn_num], layer_1_out, layer_2_preact);
    fullyconnect_activation_op<num_nodes[2]>(layer_2_preact, layer_2_out);

    // Layer 3 - dense + activation
    fullyconnect_dense_op<num_nodes[2], num_nodes[3]>(weights_layer_3[nn_num], biases_layer_3[nn_num], layer_2_out, layer_3_preact);
    fullyconnect_activation_op<num_nodes[3]>(layer_3_preact, layer_3_out);

    // Layer 4 - final dense layer
    fullyconnect_dense_op<num_nodes[3], num_nodes[4]>(weights_layer_4[nn_num], biases_layer_4[nn_num], layer_3_out, layer_4_out);

    // Output
    // Reinterpret ap_fixed as ap_int
    for(int i=0; i<num_nodes[4]; i++)
        curr_trk_invpt[i].range() = layer_4_out[0].range();
}



#endif  // __EMTF_HLSLIB_FULLYCONNECT_H__ not defined
