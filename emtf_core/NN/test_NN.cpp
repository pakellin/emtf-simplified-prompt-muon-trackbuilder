// Top function
#include <ap_int.h>
#include <ap_fixed.h>
#include <iostream>
#include <fstream>

#include "fullyconnect.h"
#include "../types.h"

// Main driver
int main(int argc, char** argv) {


    trk_feat_t nn_in[40];
    trk_invpt_t nn_out;

    for(int i=0; i<40; i++)
        nn_in[i] = 0;

    nn_in[39] = 55;
    nn_in[0] = 100;
    nn_in[1] = 100;
    nn_in[2] = 55;
    nn_in[3] = 5;

    int nn_num = 0;
    fullyconnect(nn_num, nn_in, &nn_out);

    std::cout << nn_out << std::endl;

    return 0;
}
