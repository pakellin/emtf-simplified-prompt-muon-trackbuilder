#ifndef __TRK_FINDING_CORE_H__
#define __TRK_FINDING_CORE_H__

#include <iostream>

// Xilinx HLS AP types
#include <ap_int.h>
#include <ap_fixed.h>

// Files for tf stages
#include "build_hitmaps.h"
#include "match_patterns.h"
#include "find_best_tracks.h"
#include "trk_building.h"
#include "dupe_removal.h"
#include "NN/fullyconnect.h"


// Track Finding Implementation
void trk_finding_core(int prompt_or_disp, const emtf_seg_t emtf_segs[num_sites][max_ch_site][max_seg_ch], 
                        trk_feat_t trk_features[num_tracks][num_emtf_features], trk_feat_t trk_features_rm[num_tracks][num_emtf_features], 
                        trk_invpt_t trk_invpts_out[num_tracks]){

    hitmap_t hitmaps[num_zones][num_hitmap_rows];
    build_hitmaps(emtf_segs, hitmaps);

    patt_match_t patt_matches[num_zones][num_hitmap_cols];
    match_patterns(prompt_or_disp, hitmaps, patt_matches);

    best_trk_t best_tracks[num_tracks];
    find_best_tracks(patt_matches, best_tracks);

    seg_id_t trk_seg_ids[num_tracks][max_num_layers];
    bool trk_segs_v[num_tracks][max_num_layers];
    trk_building(prompt_or_disp, best_tracks, emtf_segs, trk_features, trk_seg_ids, trk_segs_v);

    dupe_removal(trk_features, trk_seg_ids, trk_segs_v, trk_features_rm); 

    
    if(prompt_or_disp == 0){
        const int nn_num = 0;
        for (unsigned itrk=0; itrk<num_tracks; itrk++) {
            fullyconnect(nn_num, trk_features_rm[itrk], &(trk_invpts_out[itrk]));
        }  
    }
    else{
        for (unsigned itrk=0; itrk<num_tracks; itrk++) {
            trk_invpts_out[itrk] = 55;
        }
    }

}

#endif  // __MYPROJECT_H__ not defined
