#ifndef __PROMPT_TRKBUILD_DUPE_REMOVAL_H__
#define __PROMPT_TRKBUILD_DUPE_REMOVAL_H__

#include <iostream>
#include "common.h"
#include "types.h"


seg_id_t empty_seg_id(){
    seg_id_t output;
    output.isite = 0;
    output.ichamber = 0;
    output.iseg = 0;
    output.ibx = 0;

    return output;
}


void dupe_removal( const trk_feat_t trk_features[num_tracks][num_emtf_features], const seg_id_t trk_seg_ids[num_tracks][max_num_layers],
                    const bool trk_segs_v[num_tracks][max_num_layers],
                    trk_feat_t trk_features_rm[num_tracks][num_emtf_features] // quality of 0 marks it as invalid
                 ){


    // Reduce to 5 stations for duplicate removal: ME1, ME2, ME3, ME4, ME0
    //
    // site (out) | site (in)
    // -----------|-------------------------------------------
    // ME1        | ME1/1, GE1/1, ME1/2, RE1/2
    // ME2        | ME2, GE2/1, RE2/2
    // ME3        | ME3, RE3
    // ME4        | ME4, RE4
    // ME0        | ME0


    const int num_stations = 5;
    const int max_num_substations = 4;
    const int dupe_priorities[num_stations][max_num_substations] = { 
        {0,9,1,5}, 
        {2,10,6,-1}, 
        {3,7,-1,-1}, 
        {4,8,-1,-1}, 
        {11,-1,-1,-1} 
    };


    bool valid_trks[num_tracks];
    int substation_id[num_tracks][num_stations];
    seg_id_t station_segs[num_tracks][num_stations];


    // Get Highest Priority Segment for each station for all tracks
    for(int trk=0; trk<num_tracks; trk++){
        valid_trks[trk] = (trk_features[trk][38] > 0)? 1 : 0; // quality > 0 for valid tracks
        
        for(int station=0; station<num_stations; station++){
            substation_id[trk][station] = -1;
            station_segs[trk][station] = empty_seg_id();
            
            for(int sub_st=0; sub_st<max_num_substations; sub_st++){
                int layer_num = dupe_priorities[station][sub_st];
                // Choose first valid segment in priority list and move on
                if(layer_num != -1 && trk_segs_v[trk][layer_num]){
                    substation_id[trk][station] = sub_st;    
                    station_segs[trk][station] = trk_seg_ids[trk][layer_num];
                    break;
                }
            }
        }
    }


    // Invalidate some Tracks if necessary
    bool remove_trk[num_tracks] = {0};

    for(int worse_trk=1; worse_trk<num_tracks; worse_trk++){
        for(int better_trk=0; better_trk<worse_trk; better_trk++){
            // If any of the station segments in the worse track match the better track, invalidate it
            if(valid_trks[worse_trk]){
                for(int st=0; st<num_stations; st++){
                    if(substation_id[worse_trk][st] != -1 && substation_id[better_trk][st] != -1 &&
                        substation_id[worse_trk][st] == substation_id[better_trk][st] && 
                        station_segs[worse_trk][st].isite == station_segs[better_trk][st].isite &&
                        station_segs[worse_trk][st].ichamber == station_segs[better_trk][st].ichamber &&
                        station_segs[worse_trk][st].iseg == station_segs[better_trk][st].iseg &&
                        station_segs[worse_trk][st].ibx == station_segs[better_trk][st].ibx
                    ) 
                        remove_trk[worse_trk] = 1; // Remove worse track if segment ids are the same
                }
            }
        }
    }


    // Fill Output
    for(int trk=0; trk<num_tracks; trk++){
        for(int feat=0; feat<num_emtf_features; feat++){
            if(!remove_trk[trk] && valid_trks[trk])
                trk_features_rm[trk][feat] = trk_features[trk][feat]; 
            else
                trk_features_rm[trk][feat] = 0;
        }
    }
    
}


#endif
