#ifndef __EMTF_HLSLIB_NNET_KERNELS_H__
#define __EMTF_HLSLIB_NNET_KERNELS_H__

#include <cmath>  // provides std::floor, std::tanh, std::ldexp, std::abs
#include <fstream>
#include <cassert>

// EMTF HLS
#include "layer_helpers.h"


namespace detail {

template <unsigned int N, typename T, typename U>
void init_nnet_weights_op(T* arr, U op) {
  for (unsigned i = 0; i < N; i++) {
    // Cast from int to ap_int
    ap_int<T::width> w = op(i);
    // Reinterpret ap_int as ap_fixed
    arr[i].range() = w.range();
  }
}

// Adapted from:
// https://github.com/tensorflow/tensorflow/blob/master/tensorflow/core/kernels/cwise_ops.h
template <typename T = void>
float round_half_to_even(float x) {
  const float round_val = std::floor(x + 0.5f);
  const float fraction = round_val - x;
  if (__builtin_expect((fraction == 0.5f),0)) {
    return 2.0f * std::floor(0.5f * x + 0.5f);
  } else {
    return round_val;
  }
}

template <unsigned int N, typename T_IN, typename T_OUT>
void init_tanh_table_op(T_OUT table[N]) {
  static_assert(N == (1u << T_IN::width), "N value check failed");

  T_IN x = 0;

  /*
  for (unsigned i = 0; i < N; i++) {
    // Cast from unsigned to ap_uint
    ap_uint<T_IN::width> w = i;
    // Reinterpret ap_uint as ap_fixed
    x.range() = w.range();
    // Cast from ap_fixed T_IN to float32, call tanh(), cast to ap_fixed T_OUT
    float x_f32 = static_cast<float>(x);
    float y_f32 = std::tanh(x_f32);
    float q_f32 = std::ldexp(1.0f, -1 * ap_fixed_widths<T_OUT>::fwidth);
    table[i] = static_cast<T_OUT>(round_half_to_even(y_f32 / q_f32) * q_f32);
  }
  */

    // In order to match hardware use table
    std::ifstream tanh_file;
    tanh_file.open("tanh/tanh_table_4096.txt");
    if(tanh_file.is_open()){
        for(int i=0; i<N; i++){
            tanh_file >> table[i];
        }
    }
}

// Returns activation(X) using tanh
template <unsigned int N, typename T_IN, typename T_OUT>
void vector_tanh_activate_op(const T_IN x[N], T_OUT out[N]) {
  const unsigned int N_TABLE = (1u << T_IN::width);

  static bool initialized = false;
  static T_OUT tanh_table[N_TABLE];

  if (!initialized) {
    initialized = true;
    init_tanh_table_op<N_TABLE, T_IN>(tanh_table);
  }

  for (unsigned i = 0; i < N; i++) {
    // Reinterpret ap_fixed as ap_uint
    const T_IN x_i = x[i];
    const ap_uint<T_IN::width> index = x_i.range();
    assert(index < N_TABLE);
    out[i] = tanh_table[index];
  }
}

// Returns static_cast<T_OUT>(x)
template <unsigned int N, typename T_IN, typename T_OUT>
void vector_cast_op(const T_IN x[N], T_OUT out[N]) {
  static_assert(T_IN::width == T_OUT::width, "T_OUT type check faild");

  for (unsigned i = 0; i < N; i++) {
    // Cast from ap_int to ap_fixed
    const T_IN x_i = x[i];
    out[i] = static_cast<T_OUT>(x_i);
  }
}

// Returns X * Y
template <unsigned int N, typename T_IN0, typename T_IN1, typename T_OUT>
void vec_vec_mult_op(const T_IN0 x[N], const T_IN1 y[N], T_OUT out[N]) {

  constexpr int W_MULT = T_OUT::width;
  constexpr int I_MULT = T_OUT::iwidth;

  ap_fixed<W_MULT, I_MULT> mult[N];

  // Multiply
  for (unsigned i = 0; i < N; i++) {
    const T_IN0 x_i = x[i];
    const T_IN1 y_i = y[i];
    mult[i] = x_i * y_i;  // using DSP48
    out[i] = mult[i];
  }
}

// Returns dot(X, Y) + Z
// X has rows = N, Y has rows = N, Z is a scalar
template <unsigned int N, typename T_IN0, typename T_IN1, typename T_IN2, typename T_OUT>
void vec_vec_mult_biasadd_op(const T_IN0 x[N], const T_IN1 y[N], const T_IN2& z, T_OUT& out) {
  constexpr int W_MULT = AP_MIN(T_IN0::width + T_IN1::width, 24);  // capped at 24 bits (found empirically)
  constexpr int I_MULT = (T_IN0::iwidth + T_IN1::iwidth);
  constexpr int W_ACCUM = W_MULT + 4;  // additional 4 bits to prevent overflow (for 15<=x<31 accum terms)
  constexpr int I_ACCUM = I_MULT + 4;
  constexpr int W_OUT = T_OUT::width;
  constexpr int I_OUT = T_OUT::iwidth;

  ap_fixed<W_MULT, I_MULT> mult[N];

  // Multiply
  for (unsigned i = 0; i < N; i++) {
    const T_IN0 x_i = x[i];
    const T_IN1 y_i = y[i];
    mult[i] = x_i * y_i;  // using DSP48
  }

  ap_fixed<W_ACCUM, I_ACCUM> accum = z;  // init with the bias term

  // Accumulate
  for (unsigned i = 0; i < N; i++) {
    accum += mult[i];
  }

  // Round and saturate
  out = static_cast<ap_fixed<W_OUT, I_OUT, AP_RND, AP_SAT> >(accum);

}

// Returns matmul(X, Y) + Z
// X has rows = M, Y has (rows, cols) = (M, N) Z has cols = N
template <unsigned int M, unsigned int N, typename T_IN0, typename T_IN1, typename T_IN2, typename T_OUT>
void mat_vec_mult_biasadd_op(const T_IN0 x[M], const T_IN1 y[M * N], const T_IN2 z[N], T_OUT out[N]) {
  constexpr int W_MULT = AP_MIN(T_IN0::width + T_IN1::width, 24);  // capped at 24 bits (found empirically)
  constexpr int I_MULT = (T_IN0::iwidth + T_IN1::iwidth);
  constexpr int W_ACCUM = W_MULT + 4;  // additional 4 bits to prevent overflow (for 15<=x<31 accum terms)
  constexpr int I_ACCUM = I_MULT + 4;
  constexpr int W_OUT = T_OUT::width;
  constexpr int I_OUT = T_OUT::iwidth;

  for (unsigned j = 0; j < N; j++) {
    ap_fixed<W_MULT, I_MULT> mult[M];

    // Multiply
    for (unsigned i = 0; i < M; i++) {
      const T_IN0 x_i = x[i];
      const T_IN1 y_i = y[(j * M) + i];
      mult[i] = x_i * y_i;
    }

    ap_fixed<W_ACCUM, I_ACCUM> accum = z[j];  // init with the bias term

    // Accumulate
    for (unsigned i = 0; i < M; i++) {
      accum += mult[i];
    }

    // Round and saturate
    out[j] = static_cast<ap_fixed<W_OUT, I_OUT, AP_RND, AP_SAT> >(accum);
  }

}

}  // namespace detail


#endif  // __EMTF_HLSLIB_NNET_KERNELS_H__ not defined
