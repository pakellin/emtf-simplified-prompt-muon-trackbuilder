#ifndef __EMTF_HLSLIB_LAYER_HELPERS_H__
#define __EMTF_HLSLIB_LAYER_HELPERS_H__

// EMTF HLS
#include "../common.h"
#include "../types.h"
#include "nnet_weights.h"


// NN layer
struct m_nnet_0_layer_0_tag {};
struct m_nnet_0_layer_1_tag {};
struct m_nnet_0_layer_2_tag {};
struct m_nnet_0_layer_3_tag {};
struct m_nnet_0_layer_4_tag {};


namespace detail {

// _____________________________________________________________________________
// Traits
// - nnet_num_inbound_nodes_traits
// - nnet_num_outbound_nodes_traits

template <typename Category>
struct nnet_num_inbound_nodes_traits {};

template <>
struct nnet_num_inbound_nodes_traits<m_nnet_0_layer_0_tag> {
  static const int value = num_nodes_nnet_0_layer_0;
};
template <>
struct nnet_num_inbound_nodes_traits<m_nnet_0_layer_1_tag> {
  static const int value = num_nodes_nnet_0_layer_0;
};
template <>
struct nnet_num_inbound_nodes_traits<m_nnet_0_layer_2_tag> {
  static const int value = num_nodes_nnet_0_layer_1;
};
template <>
struct nnet_num_inbound_nodes_traits<m_nnet_0_layer_3_tag> {
  static const int value = num_nodes_nnet_0_layer_2;
};
template <>
struct nnet_num_inbound_nodes_traits<m_nnet_0_layer_4_tag> {
  static const int value = num_nodes_nnet_0_layer_3;
};

template <typename Category>
struct nnet_num_outbound_nodes_traits {};

template <>
struct nnet_num_outbound_nodes_traits<m_nnet_0_layer_0_tag> {
  static const int value = num_nodes_nnet_0_layer_0;
};
template <>
struct nnet_num_outbound_nodes_traits<m_nnet_0_layer_1_tag> {
  static const int value = num_nodes_nnet_0_layer_1;
};
template <>
struct nnet_num_outbound_nodes_traits<m_nnet_0_layer_2_tag> {
  static const int value = num_nodes_nnet_0_layer_2;
};
template <>
struct nnet_num_outbound_nodes_traits<m_nnet_0_layer_3_tag> {
  static const int value = num_nodes_nnet_0_layer_3;
};
template <>
struct nnet_num_outbound_nodes_traits<m_nnet_0_layer_4_tag> {
  static const int value = num_nodes_nnet_0_layer_4;
};

// _____________________________________________________________________________
// Select ops
// - select_nnet_weight_type
// - select_nnet_preactivation_type
// - select_nnet_activation_type

template <typename Category>
struct select_nnet_weight_type {};

template <>
struct select_nnet_weight_type<m_nnet_0_layer_0_tag> {
  typedef ap_fixed<11, 1> type;
};
template <>
struct select_nnet_weight_type<m_nnet_0_layer_1_tag> {
  typedef ap_fixed<10, 4> type;
};
template <>
struct select_nnet_weight_type<m_nnet_0_layer_2_tag> {
  typedef ap_fixed<10, 4> type;
};
template <>
struct select_nnet_weight_type<m_nnet_0_layer_3_tag> {
  typedef ap_fixed<10, 4> type;
};
template <>
struct select_nnet_weight_type<m_nnet_0_layer_4_tag> {
  typedef ap_fixed<12, 3> type;
};

template <typename Category>
struct select_nnet_preactivation_type {};

template <>
struct select_nnet_preactivation_type<m_nnet_0_layer_0_tag> {
  typedef ap_fixed<12, 3> type;
};
template <>
struct select_nnet_preactivation_type<m_nnet_0_layer_1_tag> {
  typedef ap_fixed<12, 3> type;
};
template <>
struct select_nnet_preactivation_type<m_nnet_0_layer_2_tag> {
  typedef ap_fixed<12, 3> type;
};
template <>
struct select_nnet_preactivation_type<m_nnet_0_layer_3_tag> {
  typedef ap_fixed<12, 3> type;
};
template <>
struct select_nnet_preactivation_type<m_nnet_0_layer_4_tag> {
  typedef ap_fixed<12, 3> type;
};

template <typename Category>
struct select_nnet_activation_type {};

template <>
struct select_nnet_activation_type<m_nnet_0_layer_0_tag> {
  typedef ap_fixed<14, 4> type;
};
template <>
struct select_nnet_activation_type<m_nnet_0_layer_1_tag> {
  typedef ap_fixed<14, 1> type;
};
template <>
struct select_nnet_activation_type<m_nnet_0_layer_2_tag> {
  typedef ap_fixed<14, 1> type;
};
template <>
struct select_nnet_activation_type<m_nnet_0_layer_3_tag> {
  typedef ap_fixed<14, 1> type;
};
// The output node has been applied a multiplicative factor of 2^6
template <>
struct select_nnet_activation_type<m_nnet_0_layer_4_tag> {
  typedef ap_fixed<14, 1 + 6> type;
};

// _____________________________________________________________________________
// Getter ops
// - get_nnet_weights_op
// - get_nnet_biases_op

template <typename Category>
struct get_nnet_weights_op {};

template <>
struct get_nnet_weights_op<m_nnet_0_layer_0_tag> {
  inline int operator()(int i) const { return weights_nnet_0_layer_0[i]; }
};
template <>
struct get_nnet_weights_op<m_nnet_0_layer_1_tag> {
  inline int operator()(int i) const { return weights_nnet_0_layer_1[i]; }
};
template <>
struct get_nnet_weights_op<m_nnet_0_layer_2_tag> {
  inline int operator()(int i) const { return weights_nnet_0_layer_2[i]; }
};
template <>
struct get_nnet_weights_op<m_nnet_0_layer_3_tag> {
  inline int operator()(int i) const { return weights_nnet_0_layer_3[i]; }
};
template <>
struct get_nnet_weights_op<m_nnet_0_layer_4_tag> {
  inline int operator()(int i) const { return weights_nnet_0_layer_4[i]; }
};

template <typename Category>
struct get_nnet_biases_op {};

template <>
struct get_nnet_biases_op<m_nnet_0_layer_0_tag> {
  inline int operator()(int i) const { return biases_nnet_0_layer_0[i]; }
};
template <>
struct get_nnet_biases_op<m_nnet_0_layer_1_tag> {
  inline int operator()(int i) const { return biases_nnet_0_layer_1[i]; }
};
template <>
struct get_nnet_biases_op<m_nnet_0_layer_2_tag> {
  inline int operator()(int i) const { return biases_nnet_0_layer_2[i]; }
};
template <>
struct get_nnet_biases_op<m_nnet_0_layer_3_tag> {
  inline int operator()(int i) const { return biases_nnet_0_layer_3[i]; }
};
template <>
struct get_nnet_biases_op<m_nnet_0_layer_4_tag> {
  inline int operator()(int i) const { return biases_nnet_0_layer_4[i]; }
};


// _____________________________________________________________________________
// Helper function to init a lookup table
// Note: If complex assignments are used to initialize a ROM, placing the array initialization
// into a separate function allows a ROM to be inferred.
template <unsigned int N, typename T, typename U>
void init_table_op(T* arr, U op) {
  for (unsigned i = 0; i < N; i++) {
    arr[i] = op(i);
  }
}

// Helper function to init a 2D lookup table
// Note: If complex assignments are used to initialize a ROM, placing the array initialization
// into a separate function allows a ROM to be inferred.
template <unsigned int M, unsigned int N, typename T, typename U>
void init_2d_table_op(T* arr, U op) {
  for (unsigned i = 0; i < M; i++) {
    for (unsigned j = 0; j < N; j++) {
      arr[(i * N) + j] = op(i, j);
    }
  }
}

}  // namespace detail


#endif  // __EMTF_HLSLIB_LAYER_HELPERS_H__ not defined
