#ifndef __EMTF_HLSLIB_FULLYCONNECT_H__
#define __EMTF_HLSLIB_FULLYCONNECT_H__

// Function hierarchy
//
// +-- fullyconnect 
//     |-- fullyconnect_preprocessing_op
//     |-- fullyconnect_dense_op
//     |-- fullyconnect_activation_op
//     |-- fullyconnect_dense_op
//     |-- fullyconnect_activation_op
//     |-- fullyconnect_dense_op
//     |-- fullyconnect_activation_op
//     +-- fullyconnect_dense_final_op

// EMTF HLS
#include <cassert>
#include "layer_helpers.h"
#include "nnet_kernels.h"


template <typename Category, typename T_IN, typename T_OUT>
void fullyconnect_activation_op(const T_IN in0[detail::nnet_num_outbound_nodes_traits<Category>::value],
                                T_OUT out[detail::nnet_num_outbound_nodes_traits<Category>::value]) {
  const unsigned int N = detail::nnet_num_outbound_nodes_traits<Category>::value;

  detail::vector_tanh_activate_op<N>(in0, out);
}


template <typename Category, typename T_IN, typename T_OUT>
void fullyconnect_preprocessing_op(const T_IN in0[detail::nnet_num_inbound_nodes_traits<Category>::value],
                                   T_OUT out[detail::nnet_num_outbound_nodes_traits<Category>::value]) {

  typedef typename detail::select_nnet_weight_type<Category>::type weight_t;
  const unsigned int M = detail::nnet_num_inbound_nodes_traits<Category>::value;
  const unsigned int N = detail::nnet_num_outbound_nodes_traits<Category>::value;

  bool initialized = false;
  weight_t weights[N];

  if (!initialized) {
    initialized = true;
    detail::init_nnet_weights_op<N>(weights, detail::get_nnet_weights_op<Category>());
  }

  ap_fixed<T_IN::width, T_IN::width> in0_cast[N];  // cast from ap_int to ap_fixed

  assert(M == N);
  detail::vector_cast_op<N>(in0, in0_cast);
  detail::vec_vec_mult_op<N>(in0_cast, weights, out);
}


template <typename Category, typename T_IN, typename T_OUT>
void fullyconnect_dense_op(const T_IN in0[detail::nnet_num_inbound_nodes_traits<Category>::value],
                           T_OUT out[detail::nnet_num_outbound_nodes_traits<Category>::value]) {

  typedef typename detail::select_nnet_weight_type<Category>::type weight_t;
  typedef typename detail::select_nnet_weight_type<Category>::type bias_t;
  const unsigned int M = detail::nnet_num_inbound_nodes_traits<Category>::value;
  const unsigned int N = detail::nnet_num_outbound_nodes_traits<Category>::value;

  bool initialized = false;
  weight_t weights[M * N];
  bias_t biases[N];

  if (!initialized) {
    initialized = true;
    detail::init_nnet_weights_op<M * N>(weights, detail::get_nnet_weights_op<Category>());
    detail::init_nnet_weights_op<N>(biases, detail::get_nnet_biases_op<Category>());
  }

  detail::mat_vec_mult_biasadd_op<M, N>(in0, weights, biases, out);
}


template <typename Category, typename T_IN, typename T_OUT>
void fullyconnect_dense_final_op(const T_IN in0[detail::nnet_num_inbound_nodes_traits<Category>::value],
                                 T_OUT out[detail::nnet_num_outbound_nodes_traits<Category>::value]) {

  typedef typename detail::select_nnet_weight_type<Category>::type weight_t;
  typedef typename detail::select_nnet_weight_type<Category>::type bias_t;
  const unsigned int M = detail::nnet_num_inbound_nodes_traits<Category>::value;
  const unsigned int N = detail::nnet_num_outbound_nodes_traits<Category>::value;

  bool initialized = false;
  weight_t weights[M * N];
  bias_t biases[N];

  if (!initialized) {
    initialized = true;
    detail::init_nnet_weights_op<M * N>(weights, detail::get_nnet_weights_op<Category>());
    detail::init_nnet_weights_op<N>(biases, detail::get_nnet_biases_op<Category>());
  }

  assert(N == 1);
  detail::vec_vec_mult_biasadd_op<M>(in0, weights, biases[0], out[0]);
}


// _____________________________________________________________________________
// Fully connected

void fullyconnect(const trk_feat_t curr_trk_feat_rm[num_emtf_features], trk_invpt_t& curr_trk_invpt) {

  const unsigned int n_layer_0 = detail::nnet_num_outbound_nodes_traits<m_nnet_0_layer_0_tag>::value;
  const unsigned int n_layer_1 = detail::nnet_num_outbound_nodes_traits<m_nnet_0_layer_1_tag>::value;
  const unsigned int n_layer_2 = detail::nnet_num_outbound_nodes_traits<m_nnet_0_layer_2_tag>::value;
  const unsigned int n_layer_3 = detail::nnet_num_outbound_nodes_traits<m_nnet_0_layer_3_tag>::value;
  const unsigned int n_layer_4 = detail::nnet_num_outbound_nodes_traits<m_nnet_0_layer_4_tag>::value;

  // Preactivation types, only needed for the hidden dense layers
  typedef detail::select_nnet_preactivation_type<m_nnet_0_layer_1_tag>::type layer_1_preact_t;
  typedef detail::select_nnet_preactivation_type<m_nnet_0_layer_2_tag>::type layer_2_preact_t;
  typedef detail::select_nnet_preactivation_type<m_nnet_0_layer_3_tag>::type layer_3_preact_t;

  // Output types
  typedef detail::select_nnet_activation_type<m_nnet_0_layer_0_tag>::type layer_0_out_t;
  typedef detail::select_nnet_activation_type<m_nnet_0_layer_1_tag>::type layer_1_out_t;
  typedef detail::select_nnet_activation_type<m_nnet_0_layer_2_tag>::type layer_2_out_t;
  typedef detail::select_nnet_activation_type<m_nnet_0_layer_3_tag>::type layer_3_out_t;
  typedef detail::select_nnet_activation_type<m_nnet_0_layer_4_tag>::type layer_4_out_t;

  // Intermediate arrays
  layer_1_preact_t layer_1_preact[n_layer_1];
  layer_2_preact_t layer_2_preact[n_layer_2];
  layer_3_preact_t layer_3_preact[n_layer_3];
  layer_0_out_t layer_0_out[n_layer_0];
  layer_1_out_t layer_1_out[n_layer_1];
  layer_2_out_t layer_2_out[n_layer_2];
  layer_3_out_t layer_3_out[n_layer_3];
  layer_4_out_t layer_4_out[n_layer_4];

  // Layer 0 - preprocessing
  fullyconnect_preprocessing_op<m_nnet_0_layer_0_tag>(curr_trk_feat_rm, layer_0_out);

  // Layer 1 - dense + activation
  fullyconnect_dense_op<m_nnet_0_layer_1_tag>(layer_0_out, layer_1_preact);
  fullyconnect_activation_op<m_nnet_0_layer_1_tag>(layer_1_preact, layer_1_out);

  // Layer 2 - dense_1 + activation_1
  fullyconnect_dense_op<m_nnet_0_layer_2_tag>(layer_1_out, layer_2_preact);
  fullyconnect_activation_op<m_nnet_0_layer_2_tag>(layer_2_preact, layer_2_out);

  // Layer 3 - dense_2 + activation_2
  fullyconnect_dense_op<m_nnet_0_layer_3_tag>(layer_2_out, layer_3_preact);
  fullyconnect_activation_op<m_nnet_0_layer_3_tag>(layer_3_preact, layer_3_out);

  // Layer 4 - dense_final
  fullyconnect_dense_final_op<m_nnet_0_layer_4_tag>(layer_3_out, layer_4_out);

  // Output
  // Reinterpret ap_fixed as ap_int
  curr_trk_invpt.range() = layer_4_out[0].range();
}


#endif  // __EMTF_HLSLIB_FULLYCONNECT_H__ not defined
