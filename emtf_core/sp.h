#ifndef __SECTOR_PROCESSOR_H__
#define __SECTOR_PROCESSOR_H__

#include <iostream>

// Xilinx HLS AP types
#include <ap_int.h>
#include <ap_fixed.h>

// SP Files
#include "common.h"
#include "types.h"
#include "tf_core.h"
#include "PrimitiveConversion/pc_types.h"
#include "PrimitiveConversion/PrimitiveConverter.h"

// Track Finding Implementation
void sector_processor(
    const int endcap, // -1 or 1
    const int sector, // 1..6

    // Trigger Primitive Inputs
    const csc_lct_t       csc_lcts[6][9][seg_ch_csc],
    const gem_cluster_t   ge11_cl[7][gem_layers][seg_ch_gem],
    const gem_cluster_t   ge21_cl[4][gem_layers][seg_ch_gem],
    const rpc_cluster_t   rpc_cl[7][3][2][seg_ch_rpc],
    const ap_uint<7>      rpc_link_vl,
    const irpc_cluster_t  irpc_cl[2][4][seg_ch_irpc],
    const me0_lct_t       me0_lcts[5][seg_ch_me0],

    trk_feat_t trk_features_out[num_tracks][num_emtf_features], 
    trk_feat_t trk_features_rm_out[num_tracks][num_emtf_features], 
    trk_invpt_t trk_invpts_out[num_tracks]
){

    // EMTF Segments (Primitive Converter Output)
    emtf_seg_t emtf_segs[num_sites][max_ch_site][max_seg_ch];
    std::memset(emtf_segs, 0, sizeof emtf_segs);

    // Perform Primitive Conversion
    PrimitiveConverter(endcap, sector, csc_lcts, ge11_cl, ge21_cl, rpc_cl, rpc_link_vl, irpc_cl, me0_lcts, emtf_segs);

    // Prompt Track Building
    trk_finding_core(0, emtf_segs, trk_features_out, trk_features_rm_out, trk_invpts_out);

    // Displaced Track Building -- need to do something with this
    trk_feat_t disp_trk_features_out[num_tracks][num_emtf_features];
    trk_feat_t disp_trk_features_rm_out[num_tracks][num_emtf_features]; 
    trk_invpt_t disp_trk_invpts_out[num_tracks];
    trk_finding_core(1, emtf_segs, disp_trk_features_out, disp_trk_features_rm_out, disp_trk_invpts_out);

}

#endif  // SECTOR_PROCESSOR not defined
